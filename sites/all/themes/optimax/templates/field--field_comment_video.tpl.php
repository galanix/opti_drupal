<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
    <?php foreach ($items as $delta => $item): ?>
        <div class="youtube" data-embed="<?php print render($item); ?>">
            <div class="play-button"></div>
        </div>
    <?php endforeach; ?>
</div>