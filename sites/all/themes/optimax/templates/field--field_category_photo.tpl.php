<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
      
        <?php foreach ($items as $delta => $item): ?>
            <div class="field-item">
				<a href="<?php print $item["#item"]["alt"]; ?>">
					<?php print render($item); ?>
                 </a>
            </div>
        <?php endforeach; ?>
  
</div>
