<?php
$node_type = $node->type;
$add_comment_classes = "";
?>

<div class="<?php print $classes . ' ' . $zebra; ?> <?php print $add_comment_classes; ?>"<?php print $attributes; ?>>


  <?php print $picture ?>

  <div class="content"<?php print $content_attributes; ?>>
    <div class="content-message-wrapper">
      <div class="username"><?php print $author; ?>, </div>

      <div class="post-date">
        <?php
        $c_date = $content['comment_body']['#object']->created;
        print trim(date("d.m.Y H:i", $c_date));

        ?>
      </div>


      <?php if ($new): ?>
        <span class="new"><?php print $new ?></span>
      <?php endif; ?>


      <?php
      hide($content['links']);
      print render($content);
      //dsm($comment);
      ?>

      <?php if ($signature): ?>
        <div class="user-signature clearfix">
          <?php print $signature ?>
        </div>
      <?php endif; ?>



      <?php if (!empty($content['links'])): ?>
        <?php print render($content['links']) ?>
      <?php endif; ?>

    </div>
  </div>


</div> <!-- /.comment -->