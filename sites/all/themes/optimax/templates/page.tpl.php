  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
    <?php if ($main_menu): ?>
      <a href="#navigation" class="element-invisible element-focusable"><?php print t('Skip to navigation'); ?></a>
    <?php endif; ?>
  </div>
  <?php if ($page['top_menu']): ?>
    <div id="top_menu">
      <?php print render($page['top_menu']); ?>
    </div> <!-- /top_menu -->
  <?php endif; ?>
  <header id="header" role="banner" class="clearfix">
    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>
    <?php if ($main_menu || $secondary_menu || !empty($page['navigation'])): ?>
      <nav id="navigation" role="navigation" class="clearfix">

        <div id="nav_inner">
          <?php if (!empty($page['navigation'])): ?> <!--if block in navigation region, override $main_menu and $secondary_menu-->
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
          <?php if (empty($page['navigation'])): ?>
            <?php print render($main_menu_expanded); ?>
            <?php
            $block_l = module_invoke('locale', 'block_view', 'language');
            print render($block_l['content']);
            ?>
          <?php endif; ?>
        </div>
      </nav> <!-- /#navigation -->
    <?php endif; ?>
    <?php
    global $user;

    // Default to a static title.
    //$title = t('My cart');
    $cart_quantity = 0;
    // If the user actually has a cart order...
    if ($order = commerce_cart_order_load($user->uid)) {
      // Count the number of product line items on the order.
      $wrapper = entity_metadata_wrapper('commerce_order', $order);
      $cart_quantity = commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types());
      // If there are more than 0 product line items on the order...
      //if ($quantity > 0) {
      // Use the dynamic menu item title.
      //$title = format_plural($quantity, 'My cart (1 item)', 'My cart (@count items)');
      //}
    }
    ?>
    <div id="cart"><span class="n_items"><?php print $cart_quantity; ?></span></div>
    <div id="callback"></div>
    <div id="search"></div>
    <div id="nav_top"></div>
    <?php print render($page['header']); ?>
  </header> <!-- /#header -->
  <?php print $messages; ?>
  <div class="content-inner-wrapper">
	<div id="h1_wrap">
		<?php if(!drupal_is_front_page() && $breadcrumb): print $breadcrumb; endif;?>
		<?php print render($title_prefix); ?>
		<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
		<?php print render($title_suffix); ?>
	</div>
	<?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
	<section id="main" role="main" class="clearfix">
	<a id="main-content"></a>
    <?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper clearfix"><?php print render($tabs); ?></div><?php endif; ?>
    <?php print render($page['help']); ?>
    <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
    <?php print render($page['content']); ?>
  </section> <!-- /#main -->
    <?php if ($page['sidebar_second']): ?>
    <aside id="sidebar-second" role="complementary" class="sidebar clearfix">
      <div id="second_menu"><i class="fa fa-bars" aria-hidden="true"></i></div>
      <?php print render($page['sidebar_second']); ?>
    </aside>  <!-- /#sidebar-second -->
  <?php endif; ?>
    </div>
  <?php if ($page['bottom_content']): ?>
    <div id="bottom_content">
      <?php print render($page['bottom_content']); ?>
    </div>  <!-- /#bottom_content -->
  <?php endif; ?>
   <div id="scrollTop" style="display:none;"></div>
  <footer id="footer" role="contentinfo" class="clearfix">
		<div id="footer_inner">
			<div id="footer_container">
              <div id="footer_container_wrapper">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" id="logo">
                  <img src="/sites/all/themes/optimax/images/bottom_logo.png" alt="<?php print t('Home'); ?>" />
                </a>
                <!-- <?php print $feed_icons ?> -->
                  <div id="bottom_footer">
                    <div id="copy">
                      © <?php
                      $currdateblock = date('Y');
                      $begindate = 2016;
                      if ($currdateblock > $begindate) {
                        print $begindate.' - '.date('Y');
                      }
                      /*  elseif ($currdateblock == $begindate) {
                         print date('Y');
                       } */
                      else {
                        print date('Y');
                      }
                      ?>, <?php print t('Optimax.com.ua - Electroradiators from manufacturer.'); ?>
                    </div>
                    <div id="copy_developer">
                      <?php print t('Design and development:').'&nbsp;'; ?><a href="http://galanix.com" target="_blank">Galanix</a>
                    </div>
                  </div> <!-- /#bottom_footer -->
              </div>
              <?php print render($page['footer']) ?>

			</div>
		</div>
    <div id="black_bottom"></div>
  </footer> <!-- /#footer -->
  <?php
  $block_form = module_invoke('webform', 'block_view', 'client-block-1');
  echo '<div id="order_form"><div id="order_form_inner">' . render($block_form['content']) . '<div id="order_close">Close</div></div></div>';
  ?>
  <div id="site_overlay"></div>