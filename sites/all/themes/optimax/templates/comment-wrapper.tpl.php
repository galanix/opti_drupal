
<h2 class="title title-drop">Список отзывов</h2>
<section id="comments" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php print render($content['comments']);
  //dsm($content);
  ?>

  <?php if ($content['comment_form']): ?>
    <section id="comment-form-wrapper">
      <h2 class="title"><?php print t('Add new comment'); ?></h2>
      <?php print render($content['comment_form']); ?>
    </section> <!-- /#comment-form-wrapper -->
  <?php endif; ?>
</section> <!-- /#comments -->