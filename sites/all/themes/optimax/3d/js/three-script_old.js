(function($) {
	$(function(){
	
		// Массивы с id элементов в svg-фигуре
		var array_id_g = ['docs-na-stole', 'kreslo', 'stol', 'podushki', 'divan', 'stul', 'window-left', 'window-line', 'window-right', 'porog', 'dver', 'kontur-general', 'lamp-1', 'lamp-2', 'lamp-3', 'lamp-4','lamp-5', 'lamp-6', 'lamp-7', 'lamp-8', 'potolok', 'lamps', 'pol-long-lines', 'pol-short-lines', 'kover-left', 'kover-big', 'kartina-big', 'wall', 'wall-shkafchik', 'wall-bottom', 'smallroom', 'batareya', 'smallroom-window-left', 'smallroom-window-right', 'smallroom-window-3', 'smallroom-dver', 's-vykluchatel', 's-vykluchatel-2', 'smallroom-table-divan', 'smallroom-smalltable', 'smallroom-stulchiki', 'smallroom-kover', 'smallroom-minikover', 'smallroom-pol', 's-batareya-right', 's-batareya-left', 'smallrooom-kreslo', 'smallroom-kartina', 'smallroom-kontur', 's-lamps', 's-lamp-6', 's-lamp-5', 's-lamp-4', 's-lamp-3', 's-lamp-2', 's-lamp-1'];
		var array_id_1 = ['docs-na-stole', 'kreslo', 'stol', 'podushki', 'divan', 'stul', 'window-right', 'window-line', 'window-left'];
		var array_id_2 = ['porog', 'dver', 'kontur-general', 'lamps', 'lamp-1', 'lamp-2', 'lamp-3', 'lamp-4','lamp-5', 'lamp-6', 'lamp-7', 'lamp-8', 'potolok', 'pol-long-lines', 'pol-short-lines', 'kover-left', 'kover-big'];
		var array_id_3 = ['kartina-big', 'wall', 'wall-shkafchik', 'wall-bottom', 'smallroom', 'batareya', 'smallroom-window-left', 'smallroom-window-right', 'smallroom-window-3', 'smallroom-dver', 's-vykluchatel', 's-vykluchatel-2', 'smallroom-table-divan', 'smallroom-smalltable', 'smallroom-stulchiki', 'smallroom-kover', 'smallroom-minikover', 'smallroom-pol', 's-batareya-right', 's-batareya-left', 'smallrooom-kreslo', 'smallroom-kartina', 'smallroom-kontur', 's-lamps', 's-lamp-6', 's-lamp-5', 's-lamp-4', 's-lamp-3', 's-lamp-2', 's-lamp-1'];
		var img_div = $('#img'),
			 svg_div = $('#svg'),
			 img = img_div.find('img.room'),
			 front_content = $('#front_content'),
			 preloader = $('#preloader'),
			 points_wrapper = $('#points_wrapper');
		
		replaceWithPaths($('svg'));
		getLenghtSVGPaths('svg');
		
	
	// Проверка поддержки браузером WebGL
   if ( !Detector.webgl ) {
	   // Detector.addGetWebGLMessage(); // стандартная страница-уведмление о том, что WebGL браузером не поддерживается
		
		svg_div.css({
			opacity: 1
		});
		points_wrapper.hide();
		
		
		// Загрузка скриптов и стилей для слайдера
		// dhtmlLoadScript("/sites/all/themes/optimax/frontpage/js/slick.min.js");
		dhtmlLoadCSS("/sites/all/themes/optimax/frontpage/css/slick.css");
		dhtmlLoadCSS("/sites/all/themes/optimax/frontpage/css/slick-theme.css");
		
		// Через $ функцию load загрузить контент со слайдшоу из отделного html-файлика, вставить его в документ и подключить слайдер
		var front_slider = $("#front_slider");
		front_slider.load("/sites/all/themes/optimax/frontpage/slider.html", function(){
			// console.log(front_slider);
			$( this ).slick({
				autoplay: false,
				dots: true,
				arrows: false,
				fade: true,
				autoplaySpeed: 5000,
				speed: 1000
			 });
			$( this ).slick('slickGoTo', 1);
			 
			setTimeout( function(){
				preloader.hide();
				startSVGAnimation(array_id_g, array_id_1, array_id_2, array_id_3);
				
				
			}, 1000);
			
			
		});
	}
   else {
	  
	  // При поддержке браузером WebGL инициализируем нужные переменные и запускаем объяывленные функции init() и animate()
	  var container;
      var stats;
      var camera, controls, scene, renderer;
      var mesh;
      var helper;
      var mouseX = 0, mouseY = 0;
      var clock = new THREE.Clock();
      var windowHalfX = window.innerWidth / 2;
      var windowHalfY = window.innerHeight / 2;

      init();
      animate();
	   
   }
   
   // Динамическая загрузка скрипта на странице по его url
	function dhtmlLoadScript(url) {
	   var e = document.createElement("script");
	   e.src = url;
	   e.type="text/javascript";
	   document.getElementsByTagName("head")[0].appendChild(e); 
	}
	// Динамическая загрузка файла со стилями на странице по его url
	function dhtmlLoadCSS(url) {
		var link = document.createElement('link');
		link.setAttribute('rel', 'stylesheet');
		link.setAttribute('type', 'text/css');
		link.setAttribute('media', 'all');
		link.setAttribute('href', url)
		document.getElementsByTagName('head')[0].appendChild(link);
	}
	
	
	// Функция с настройками и описанием камеры, сцены, 3D-объектов и др.
	function init() {

        container = document.getElementById( 'container' );
		
		renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
        renderer.setClearColor( 0x000000, 0 );
        renderer.setSize( window.innerWidth, window.innerHeight );

        container.appendChild( renderer.domElement );

        camera = new THREE.PerspectiveCamera( 35, window.innerWidth / window.innerHeight, 1, 1500000 );
        camera.position.set( 0,100, 300 );
			
        scene = new THREE.Scene();
			

        // ambient light
        // ambLight = new THREE.AmbientLight( 0x444444 );
       ambLight = new THREE.AmbientLight( 0x585858 );
       scene.add( ambLight );

        // directional - KEY LIGHT
        keyLight = new THREE.DirectionalLight( 0xdddddd, .7 );
        keyLight.position.set( -80, 60, 80 );
        scene.add( keyLight );
        
        keyLightHelper = new THREE.DirectionalLightHelper( keyLight, 15 );
        // scene.add( keyLightHelper );

        // directional - FILL LIGHT
        fillLight = new THREE.DirectionalLight( 0xdddddd, .3);
        fillLight.position.set( 80, 40, 40 );
        scene.add( fillLight );
        
         fillLightHelper = new THREE.DirectionalLightHelper( fillLight, 15 );
         // scene.add( fillLightHelper );

        // directional - RIM LIGHT
        rimLight = new THREE.DirectionalLight( 0xdddddd, .7 );
        rimLight.position.set( -20, 80, -80 );
        scene.add( rimLight );
        
         rimLightHelper = new THREE.DirectionalLightHelper( rimLight, 15 );
         // scene.add( rimLightHelper );

		
		var axisHelper = new THREE.AxisHelper( 5 );
		// scene.add( axisHelper );
			
        // var helper = new THREE.GridHelper( 120, 20 );
        // scene.add( helper );
      
		var specularShininess = Math.pow( 2, 1 * 10 );
				
        var section_material = new THREE.MeshPhongMaterial({
			color: 0xfffce8,
			side: THREE.DoubleSide,
			shininess: specularShininess,
			reflectivity: 0.5
		});
		var section_inner_material = new THREE.MeshPhongMaterial({
			color: 0xfffce8,
			side: THREE.DoubleSide,
			shininess: specularShininess,
			reflectivity: 0.5,
			transparent: true,
			opacity: 0
		});
        var white_material = new THREE.MeshPhongMaterial({ color: 0xffffff, side: THREE.DoubleSide });
		var black_material = new THREE.MeshPhongMaterial({ color: 0x000000, side: THREE.DoubleSide });
		var yellow_material  = new THREE.MeshPhongMaterial({ color: 0xffff00, side: THREE.DoubleSide });
		var gold_material  = new THREE.MeshPhongMaterial({
			color: 0xe2e29b,
			side: THREE.DoubleSide,
			shininess: specularShininess,
			reflectivity: 1
		 });
		 var ten_material  = new THREE.MeshPhongMaterial({
			color: 0x947055,
			side: THREE.DoubleSide,
			shininess: Math.pow( 2, 1 * 5 ),
			reflectivity: 0.5
		 });
		
		
		
		
		
    	// Создание общей группы радиатора 
		group = new THREE.Object3D();
		
		// Загрузчик файла со сценой и обработка его
		var loader = new THREE.ObjectLoader();
        loader.load( "/sites/all/themes/optimax/3d/models/batareya-sprite-inner.json", function( geometry ) {
			
			
			 
			svg_div.css({
				opacity: 1
			});
		
			
			
			startSVGAnimation(array_id_g, array_id_1, array_id_2, array_id_3);
			
			
			
			
			// Вызов функции генерации 3D-объекта радиатора
			getRadiator(geometry);
		
			// После загрузки файла скрываем прелоадер и показываем контейнер с 3D-сценой
			var container_div = document.getElementById('container');
			var load_el = document.getElementById('loader');
			var pre_load_el = document.getElementById('preloader');
			// container_div.style.display = "block";
			pre_load_el.style.display = "none";
		
        },
		// Функция, которая вызывается во время загрузки файла, до его завершения
		function ( xhr ) {
			var load_text =  Math.round(xhr.loaded / xhr.total * 100) + '% Загружено';
			var load_el = document.getElementById('loader');
			load_el.innerHTML = load_text;
			// Загружено 100% - можно просто показывать в тот момент, когда 3d-объект батареи уже загружен, а svg-ещё догружается, делать проверку типа svg.load
		}
		
		);
		
		// Создание 3D-объекта радиатора и добавление его на сцену
		function getRadiator(geometry) {
			// Выбор отдельных элементов сцены, которая была загружена из файла
			var section_mesh = geometry.getObjectByName("section");
			var electro_box = geometry.getObjectByName("electro-box");
			var btn1 = geometry.getObjectByName("btn1");
			var btn2 = geometry.getObjectByName("btn2");
			var btn3 = geometry.getObjectByName("btn3");
			var box_label = geometry.getObjectByName("label");
			
			
			var bot_kran1 = geometry.getObjectByName("bot-kran1");
			var bot_kran2 = geometry.getObjectByName("bot-kran-2");
			
			var top_kran_1 = geometry.getObjectByName("top-kran-1");
			var top_kran_2 = geometry.getObjectByName("top-kran-2");
			var top_kran_3 = geometry.getObjectByName("top-kran-3");
			var top_kran_4 = geometry.getObjectByName("top-kran-4");
			var top_kran_5 = geometry.getObjectByName("top-kran-5");
			
			var bot_shnur_1 = geometry.getObjectByName("bot-shnur-1");
			var bot_shnur_2 = geometry.getObjectByName("bot-shnur-2");
			var bot_shnur_3 = geometry.getObjectByName("bot-shnur-3-vilka");
			
			var top_shnur_1 = geometry.getObjectByName("top-shnur-1");
			var top_shnur_2 = geometry.getObjectByName("top-shnur-2-provolka");
			var top_shnur_3 = geometry.getObjectByName("top-shnur-3-datchik");
			var top_shnur_4 = geometry.getObjectByName("top-shnur-4");
			
			var section_inner_mesh = geometry.getObjectByName("section-inner");
			
			var top_inner_cylinder = geometry.getObjectByName("top-inner-cylinder");
			var bot_inner_cylinder = geometry.getObjectByName("bot-inner-cylinder");
			
			var top_inner_gaika = geometry.getObjectByName("top-inner-gaika");
			var bot_inner_gaika = geometry.getObjectByName("bot-inner-gaika");
			
			var ten_mesh = geometry.getObjectByName("ten");
			
			
			// Добавление материалов к элементам сцены
			electro_box.material = section_material;
			btn1.material = black_material;
			btn2.material = black_material;
			btn3.material = black_material;
									
			bot_kran1.material = white_material;
			bot_kran2.material = white_material;
			
			top_kran_1.material = white_material;
			top_kran_2.material = white_material;
			top_kran_3.material = gold_material;
			top_kran_4.material = white_material;
			top_kran_5.material = gold_material;
			
			bot_shnur_1.material = section_material;
			bot_shnur_2.material = section_material;
			bot_shnur_3.material = section_material;
			
			top_shnur_1.material = section_material;
			top_shnur_2.material = section_material;
			top_shnur_3.material = section_material;
			top_shnur_4.material = section_material;
			
			top_inner_gaika.material = gold_material;
			bot_inner_gaika.material = gold_material;
			ten_mesh.material = ten_material;
			top_inner_cylinder.material = white_material;
			bot_inner_cylinder.material = white_material;
			
			
			// Создание групп для объектов сцены
			group_top_kran = new THREE.Object3D();
			group_bot_kran = new THREE.Object3D();
			group_top_shnur = new THREE.Object3D();
			group_bot_shnur = new THREE.Object3D();
			group_inner = new THREE.Object3D();
			group_inner_sections = new THREE.Object3D();
			
			// Добавление объектов в группы
			group_top_kran.add(top_kran_1);
			group_top_kran.add(top_kran_2);
			group_top_kran.add(top_kran_3);
			group_top_kran.add(top_kran_4);
			group_top_kran.add(top_kran_5);
			
			group_bot_kran.add(bot_kran1);
			group_bot_kran.add(bot_kran2);
			
			group_top_shnur.add(top_shnur_1);
			group_top_shnur.add(top_shnur_2);
			group_top_shnur.add(top_shnur_3);
			group_top_shnur.add(top_shnur_4);
			
			group_bot_shnur.add(bot_shnur_1);
			group_bot_shnur.add(bot_shnur_2);
			group_bot_shnur.add(bot_shnur_3);
			
			group_inner.add(ten_mesh);
			group_inner.add(top_inner_cylinder);
			group_inner.add(bot_inner_cylinder);
			
			
			// Корректировка положения групп объектов
			group_top_kran.position.x -= 29.3;
			group_top_kran.position.z -= 0.3;
			group_top_kran.position.y += 0.075;
			
			group_bot_kran.position.x -= 29.32;
			group_bot_kran.position.z -= 0.35;
			group_bot_kran.position.y += 0.075;
			
			group_top_shnur.position.x -= 0.564;
			group_bot_shnur.position.x -= 0.564;
			
			group_inner.position.x -= 29.3;
			group_inner.position.y += 0.1;
			group_inner.position.z -= 0.334;
						
			electro_box.position.x -= 0.564;
			btn1.position.x -= 0.564;
			btn2.position.x -= 0.564;
			btn3.position.x -= 0.564;
			
			// Заргузка текстуры этикетки и добавление этикетки в группу радиатора
			var loader_label_texture = new THREE.TextureLoader();
			loader_label_texture.load(
				'/sites/all/themes/optimax/3d/textures/etiketka.png',
				function ( texture ) {
					var label_material = new THREE.MeshBasicMaterial({ map:texture, overdraw: true, side: THREE.DoubleSide });
					box_label.material = label_material;
					box_label.position.x -= 0.564;
					box_label.position.z += 0.002;
					group.add(box_label);
				}
			);
			
			// Добавление объектов и групп в общую группу радиатора
			group.add(electro_box);
			group.add(btn1);
			group.add(btn2);
			group.add(btn3);
			group.add(box_label);
					
			group.add(group_bot_kran);
			group.add(group_top_kran);
			group.add(group_top_shnur);
			group.add(group_bot_shnur);
			
			group.add(group_inner);
					
			
			// Размножение секций радиатора и добавление их в группу радиатора
			 for (var j = -5 ; j < 5; j++) {
			
				var new_section_mesh = new THREE.Mesh( section_mesh.geometry, section_material );
			
				new_section_mesh.position.x = 3.2*j;
				new_section_mesh.position.z = 10;
				new_section_mesh.rotation.y = -0.138 + Math.PI;
				new_section_mesh.name = "section_" + (j + 5);
			   
			    group.add( new_section_mesh );//add a mesh with geometry to it
			}
			
			// Размножение секций в разрезе
			 for (var jj = -5  ; jj < -2; jj++) {
			
				var new_inner_section_mesh = new THREE.Mesh( section_inner_mesh.geometry, section_inner_material );
			
				new_inner_section_mesh.position.x = - 0.095 + 0.319 * jj;
				new_inner_section_mesh.name = "section_inner_" + (jj + 5);
				group_inner_sections.add( new_inner_section_mesh );
			}
			
			// Размножение внутренних верхних и нижних гаек
			 for (var jjj = -5  ; jjj < -2; jjj++) {
			
				var new_top_inner_gaika_mesh = new THREE.Mesh( top_inner_gaika.geometry, gold_material );
				var new_bot_inner_gaika_mesh = new THREE.Mesh( bot_inner_gaika.geometry, gold_material );
			
				new_top_inner_gaika_mesh.position.x = - 0.095 + 0.319 * jjj;
				new_bot_inner_gaika_mesh.position.x = - 0.095 + 0.319 * jjj;
				new_top_inner_gaika_mesh.name = "top_inner_gaika_" + (jjj + 5);
				new_bot_inner_gaika_mesh.name = "bot_inner_gaika_" + (jjj + 5);
			   			   
			   group_inner_sections.add( new_top_inner_gaika_mesh );
			   group_inner_sections.add( new_bot_inner_gaika_mesh );
			}
		
			// Корректировка положения внутренней группы и добавление её в общую группу радиатора
			group_inner_sections.rotation.x -= 0.5 * Math.PI;
			group_inner_sections.position.z += 1.68;
			group_inner_sections.scale.set(10,10,10);
			group.add(group_inner_sections);
			
			
			// Корректировка положения общей группы радиатора и добавление её на сцену
			group.scale.set(4,4,4);
			group.position.y = -30;
			group.position.x = 55;
			group.rotation.y = 0.45;
			group.name = "radiator_group"
			scene.add( group );
		}
		
		
		// Функция, возвращающая объект с текстами подписей и настройки их отображения
		function getTextPlaneInfo() {
			var points_text_obj = {
				"point_1": {
					text: "Возможность программирования от 1°C до 30°С по помещению на каждый час недели (таймер)",
					location: new THREE.Vector3( -11, 50, 50 ),
					camera_position: new THREE.Vector3( 171.21, 74.00, 96.02 ),
					camera_look_at: new THREE.Vector3( 0, 0, 0 ),
					width: 22,
					rotation_y: 0.9,
					show_inner_sections: false
				},
				"point_2": {
					text: "Биметаллические секции глубиной 96 мм",
					location: new THREE.Vector3( -20, 30, 40 ),
					camera_position: new THREE.Vector3( 88.73, 27.15, 226.41 ),
					camera_look_at: new THREE.Vector3( 0, 0, 0 ),
					width: 10,
					show_inner_sections: false
				},
				"point_3": {
					text: "Тен из нержавеющей стали индивидуальной мощности из расчёта 120 Вт на секцию",
					location: new THREE.Vector3( -15, -8, 61 ),
					camera_position: new THREE.Vector3( -42.01, 23.29, 193.62 ),
					width: 15,
					rotation_y: -0.32,
					show_inner_sections: true
				},
				"point_4": {
					text: "Прибор нагревается максимум до 70 °С, не сушит воздух и не жжёт кислород",
					location: new THREE.Vector3( -20, 30, -5  ),
					camera_position: new THREE.Vector3( -107.12, 10.53, -180.07 ),
					width: 25,
					rotation_y: Math.PI + 0.45,
					show_inner_sections: false
					
				},
				"point_5": {
					text: "Датчик с точностью до 0,5 °С снимает температуру помещения",
					location: new THREE.Vector3( -48, 47, 31 ),
					camera_position: new THREE.Vector3( 92.10, 91.43, -207.42 ),
					width: 20,
					rotation_y: Math.PI - 0.32,
					show_inner_sections: false
				},
				"point_6": {
					text: "На радиаторной части прибора можно сушить вещи любой влажности",
					location: new THREE.Vector3( -20, 70, 40 ),
					camera_position: new THREE.Vector3( 80.04, 166.49, 160.44 ),
					width: 25,
					//rotation_y: -0.5,
					show_inner_sections: false
				},
				"point_7": {
					text: "Инертная, безвредная, жидкость - антифриз на основе пропиленгликоля",
					location: new THREE.Vector3( -15, 10, 61 ),
					camera_position: new THREE.Vector3( -40.44, 51.11, 211.22 ),
					width: 15,
					rotation_y: -0.30,
					show_inner_sections: true
				},
				"point_8": {
					text: "Электрорадиатор сертифицирован в системе УкрСЕПРО",
					location: new THREE.Vector3( -30, 45, 55 ),
					camera_position: new THREE.Vector3( -114.26, 79.39, 99.46 ),
					width: 10,
					rotation_y: -0.75,
					show_inner_sections: false
				}
			}
			return points_text_obj;
		}
		
		// Обёрточный элемент для управляющих кнопок
		var points_wrapper = $("#points_wrapper");
		
		// Загрузка файла со шрифтом и обработка появления текстовых подписей
		var f_loader = new THREE.FontLoader();
		var font = f_loader.load(
			'/sites/all/themes/optimax/3d/fonts/droid/droid_sans_mono_regular.typeface.json', function (font) {
				
				// Обработка клика по кнопкам слева на экране
				points_wrapper.find("div").on("click", function() {
									
					var selected_section_0 = scene.getObjectByName("section_0");
					var selected_section_1 = scene.getObjectByName("section_1");
					var selected_section_2 = scene.getObjectByName("section_2");
					
					var selected_inner_section_0 = scene.getObjectByName("section_inner_0");
					var selected_inner_section_1 = scene.getObjectByName("section_inner_1");
					var selected_inner_section_2 = scene.getObjectByName("section_inner_2");
					
					var current_point = $( this );
					var id = current_point.attr("id");
					var text_info_obj = getTextPlaneInfo()[id];
					var selectedObject = scene.getObjectByName("text_plane");
					scene.remove( selectedObject );
					
					if (current_point.hasClass("active")) {
						current_point.removeClass("active");
					}
					else {
						points_wrapper.find("div").removeClass("active");
						current_point.addClass("active");
					
						addLabel({
							text: text_info_obj.text,
							font: font,
							rotation_y: text_info_obj.rotation_y || 0.45,
							location: text_info_obj.location,
							width: text_info_obj.width
						});
						
						
					   if (text_info_obj.show_inner_sections) {
							showSection(selected_inner_section_0);
							showSection(selected_inner_section_1);
							showSection(selected_inner_section_2);
						 }
						  
						  
						  var l_tween = new TWEEN.Tween(camera.position).to({
								x: text_info_obj.camera_position.x,
								y: text_info_obj.camera_position.y,
								z: text_info_obj.camera_position.z
							}, 1550 )
							.easing(TWEEN.Easing.Linear.None)
							.onUpdate(function (i) {
								
								 var m_opacity = 1 - i;
								 
								 // в идеале нужно хранить состояние того, скрыты ли секции сейчас, и в зависимостит от этого показывать их или нет
								 if (text_info_obj.show_inner_sections) {
									hideSection(selected_section_0, m_opacity);
									hideSection(selected_section_1, m_opacity);
									hideSection(selected_section_2, m_opacity);
								 }
								 else {
									showSection(selected_section_0);
									showSection(selected_section_1);
									showSection(selected_section_2);
								 }
							})
							.onComplete(function () {
								 if (!text_info_obj.show_inner_sections) {
									 hideSection(selected_inner_section_0, 0);
									hideSection(selected_inner_section_1, 0);
									hideSection(selected_inner_section_2, 0);
								 }
							})
							.start();
							
					}
				});
	
	
				// Добавление надписи на кнопке ON / OFF
				var group_label_text = new THREE.Object3D();
				var label_text_material = new THREE.MeshBasicMaterial( { color: 0xffffff } );
				var label_text = new THREE.TextGeometry( "ON", {
					font: font,
					size: 0.41,
					height: 0.1,
					curveSegments: 12,
				});
				var label_text2 = new THREE.TextGeometry( "OFF", {
					font: font,
					size: 0.41,
					height: 0.1,
					curveSegments: 12,
				});
				var label_text_mesh = new THREE.Mesh( label_text, label_text_material );
				var label_text_mesh2 = new THREE.Mesh( label_text2, label_text_material );
				label_text_mesh.position.copy( new THREE.Vector3( 0, 70, 0 ) );
				label_text_mesh2.position.copy( new THREE.Vector3( 0, 70, 0 ) );
				label_text_mesh.name = "on";
				label_text_mesh2.name = "off";
				label_text_mesh2.position.y -= 1.5;
				
				group_label_text.add( label_text_mesh );
				group_label_text.add( label_text_mesh2 );
				group_label_text.rotation.y += Math.PI + 0.44;
				group_label_text.position.y -= 42.5;
				group_label_text.position.x += 58.95;
				group_label_text.position.z -= 11.1;
							
				scene.add(group_label_text);
	
			}
	
		);
		
		
		// можно вообще заменить на setSectionOpacity
		function hideSection(section, opacity) {
			section.material =  new THREE.MeshPhongMaterial({
				color: 0xfffce8,
				side: THREE.DoubleSide,
				 shininess: specularShininess,
				 reflectivity: 0.5,
				 transparent: true,
				 opacity: opacity
			});
		}
		function showSection(section) {
			section.material =  new THREE.MeshPhongMaterial({
				color: 0xfffce8,
				side: THREE.DoubleSide,
				 shininess: specularShininess,
				 reflectivity: 0.5,
				 transparent: false, // очень важно его возвращать, а то он делает прозначными и элементы под ним ))
				 opacity: 1
			});
		}

		
		/**
		* Функция создания подписи на фоне прямоугольника
		*/
		function addLabel( params ) {
			
			// Входящие параметры функции
			var text = params.text;
			var title = params.title || "text_plane";
			var font = params.font;
			var location = params.location;
			var width = params.width;
			var font_size = params.fontsize || 1.4;
			var font_color = params.font_color || 0xffffff;
			var group_rotation_y = params.rotation_y || 0.45;
			var groupT = new THREE.Object3D();
			groupT.name = title;
			
			// Используем функцию генерации под строк из одной большой строки
			var str_array = canvasWordWrap(text, width, font_size );
			for (aa in str_array) {
				var textGeo = new THREE.TextGeometry( str_array[aa], {
					font: font,
					size: font_size,
					height: 0.1,
					curveSegments: 12
				});
				var textMaterial = new THREE.MeshBasicMaterial( { color: font_color } );
				var textMesh = new THREE.Mesh( textGeo, textMaterial );
				textMesh.position.copy( location );
				textMesh.position.y = location.y - aa*2;
				
				groupT.add( textMesh );
			}
			
			// определяем высоту плоскости
			var text_line_height = 2.5;
			var plane_text_height = str_array.length * text_line_height;
					
			var planeGeometryT = new THREE.PlaneGeometry(width*2,plane_text_height,10,10);
			var planeMaterialT = new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.7, side: THREE.DoubleSide });
			var planeT = new THREE.Mesh(planeGeometryT,planeMaterialT);
			
			planeT.position.x = location.x +width - 2;
			planeT.position.y = location.y - plane_text_height/2 + 2.5;
			planeT.position.z = location.z;
					
			groupT.add(planeT);
			
			var planeGeometryT2 = new THREE.PlaneBufferGeometry(width*2,plane_text_height,1, 1);
			var edgesT2 = new THREE.EdgesGeometry( planeGeometryT2 );
			var planeMaterialT2 = new THREE.LineBasicMaterial( { color: 0xffffff } );
			var planeT2 = new THREE.LineSegments(edgesT2, planeMaterialT2);
			
			planeT2.position.x = location.x +width - 2;
			planeT2.position.y = location.y - plane_text_height/2 + 2.5;
			planeT2.position.z = location.z;
			
			groupT.add(planeT2);
			groupT.rotation.y = group_rotation_y;
			
			scene.add( groupT );
		}

	
		// Добавление сфер-меток. Нужно вынести в отдельную удобную функцию
		/* var geometryS = new THREE.SphereGeometry( 2, 32, 32 );
		//var materialS = new THREE.MeshBasicMaterial( {color: 0xffff00} );
		 var materialS = new THREE.MeshPhongMaterial({ color: 0xffff00, side: THREE.DoubleSide });
		var sphereS = new THREE.Mesh( geometryS, materialS );
		var s_location = new THREE.Vector3( -48, 0, 32 ) 
		sphereS.position.copy( s_location );
		sphereS.name = "yellow_s"
		scene.add( sphereS ); */
		
		
		/*
		* @param {string} Входная строка
		* @param {limit} Ограничения по к-ву пикселей
		* @param {fontSize} Размер шрифта
		*/
		function canvasWordWrap(string, limit, fontSize) {

			// Входные данные и шаблоные данные для обработки строки
			var text = string,
			limit = limit,
			fonstSize = fonstSize,
			newStringArray = [],
			stringLength = 0,
			stringTemplate = '';

			// Инициализация канвас объекта с учетом входящего размера шрифта
			var canvasTemplate = document.createElement('canvas'),
			canvasTemplateContext = canvasTemplate.getContext('2d');
			canvasTemplateContext.font = fontSize + 'px serif';

			// Подготовка строки для обработки
			var stringArr = text.trim().split(' '),
			stringArrLength = stringArr.length - 1,
			stringArrLengthFull = stringArr.length;

			// Цикл присвоения нужных пробелов для слов, кроме последнего
			for(var i = 0; stringArrLength > i; i++){
				stringArr[i] = stringArr[i] + ' ';
			}

			// Основной метод разделения строки на подстроки
			for (var i = 0; stringArrLengthFull > i; i++){
				stringLength += canvasTemplateContext.measureText(stringArr[i]).width; // нынешняя длина строки
				
				if (stringLength < limit){ // проверка строки на превешение лимита
					stringTemplate += stringArr[i]; 
					if (stringArrLength == i){
						newStringArray.push(stringTemplate);    
					}
				}
				
				else {
					if (stringTemplate != '') {
						newStringArray.push(stringTemplate);
					}
					stringLength = canvasTemplateContext.measureText(stringArr[i]).width;
					stringTemplate = stringArr[i];
					if (stringArrLength == i) {
						newStringArray.push(stringTemplate);    
					}
				}
				
			}
			return newStringArray;
		}
			
		
		var reflectionCube = new THREE.CubeTextureLoader()
					.setPath( '/sites/all/themes/optimax/3d/textures/room-panoram/' )
					.load( [ 'px.jpg', 'nx.jpg', 'py.jpg', 'ny.jpg', 'pz.jpg', 'nz.jpg' ] );
				reflectionCube.format = THREE.RGBFormat;
				//scene = new THREE.Scene();
				scene.background = reflectionCube;
	
		

		
		
		var planeGeometry = new THREE.PlaneGeometry(1500,1500,20,20);
		
		// instantiate a loader
		var loader_texture = new THREE.TextureLoader();

		// load a resource
		loader_texture.load(
			// resource URL
			'/sites/all/themes/optimax/3d/textures/basketball-floor-texture.jpg',
			// Function when resource is loaded
			function ( texture ) {
							
				texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
				texture.offset.set( 0, 0 );
				texture.repeat.set( 5, 5 );
				
				
				var planeMaterial = new THREE.MeshBasicMaterial({map:texture, overdraw: true});
				var plane = new THREE.Mesh(planeGeometry,planeMaterial);
				plane.rotation.x=-0.5*Math.PI;
				plane.position.x = 0;
				plane.position.y = -30;
				plane.position.z = 0;
				scene.add(plane);
			});
			
	
        // setupTween();
        controls = new THREE.OrbitControls( camera, renderer.domElement );
        controls.enableKeys = false;
		controls.maxPolarAngle = 1.62;
		// controls.maxAzimuthAngle
		// controls.minDistance
		controls.maxDistance = 500;
		controls.enablePan = false;
		// console.log(controls);

		// This class provides a simple info box that will help you monitor your code performance.
		// https://github.com/mrdoob/stats.js/
        /* stats = new Stats();
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.top = '0px';
        container.appendChild( stats.domElement ); */
        // document.addEventListener( 'mousemove', onDocumentMouseMove, false );
        window.addEventListener( 'resize', onWindowResize, false );
      }

      function setupGui() {
        // dat.GUI debugging
        var gui = new dat.GUI();
        var f1 = gui.addFolder('meshOuter rotation');
        f1.add(mesh.position, 'x', 0, 2*Math.PI);
        f1.add(mesh.position, 'y', 0, 2*Math.PI);
        f1.add(mesh.position, 'z', 0, 2*Math.PI);
        f1.open();
      }


      function onWindowResize() {
        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );
		// console.log(camera.position);
      }

      function onDocumentMouseMove( event ) {
        mouseX = ( event.clientX - windowHalfX );
        mouseY = ( event.clientY - windowHalfY );
        // document.getElementById('mouse').innerText = mouseX + ', ' + mouseY;
      }

      // var paused = false;
      function animate() {
       
       // if (!paused) {
	      render();
          // stats.update();
          controls.update();
          // TWEEN.update();
        // }
		requestAnimationFrame( animate );
      }

      function render() {
		// var delta = clock.getDelta();
		// Задаём изначальный поворот и угол обзора батареи
        if (group) {
			// mesh.rotation.y -= 0.5 * delta;
			//mesh.rotation.y = 3.55;
			//group.rotation.y = 3.55;
			//mesh.rotation.x = -0.3;
        }
		
		renderer.render( scene, camera );
		TWEEN.update();
      }


      function setupTween(obj, prop, targetValue) {
        var update  = function(){
          obj[prop] = current.property;
        }
        var current = { property: obj[prop] };
        var target = { property: targetValue };
        var tween = new TWEEN.Tween(current).to(target, 800)
          .easing(TWEEN.Easing.Cubic.Out)
          .onUpdate(update);
        tween.start();
      }
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  /*
	  * Вспомогательные функции для создания отрисовки векторного svg-контура комнаты
	  */
	  	
		// Easing excerpt from George McGinley Smith 
	// http://gsgd.co.uk/sandbox/jquery/easing/
	jQuery.extend( jQuery.easing,
	{
		easeInOutQuad: function (x, t, b, c, d) {
			if ((t/=d/2) < 1) return c/2*t*t + b;
			return -c/2 * ((--t)*(t-2) - 1) + b;
	  }
	});


	function SVG(tag) {
		return document.createElementNS('http://www.w3.org/2000/svg', tag);
	}



	function replaceRectsWithPaths(parentElement) {


		var rects = $(parentElement).find('rect');

		$.each(rects, function() {

			var rectX = $(this).attr('x');
			var rectY = $(this).attr('y');

			var rectX2 = parseFloat(rectX) + parseFloat($(this).attr('width'));
			var rectY2 = parseFloat(rectY) + parseFloat($(this).attr('height'));

			var convertedPath = 'M' + rectX + ',' + rectY + ' ' + rectX2 + ',' + rectY + ' ' + rectX2 + ',' + rectY2 + ' ' + rectX + ',' + rectY2 + ' ' + rectX + ',' + rectY;


			$(SVG('path'))
			.attr('d', convertedPath)
			.attr('fill', $(this).attr('fill'))
			.attr('stroke', $(this).attr('stroke'))
			.attr('stroke-width', $(this).attr('stroke-width'))
			.attr('class', $(this).attr('class'))
			.insertAfter(this);

		});

		$(rects).remove();
	}



	function replaceLinesWithPaths(parentElement) {


		var lines = $(parentElement).find('line');

		$.each(lines, function() {

			var lineX1 = $(this).attr('x1');
			var lineY1 = $(this).attr('y1');

			var lineX2 = $(this).attr('x2');
			var lineY2 = $(this).attr('y2');

			var convertedPath = 'M' + lineX1 + ',' + lineY1 + ' ' + lineX2 + ',' + lineY2;


			$(SVG('path'))
			.attr('d', convertedPath)
			.attr('fill', $(this).attr('fill'))
			.attr('stroke', $(this).attr('stroke'))
			.attr('stroke-width', $(this).attr('stroke-width'))
			.attr('class', $(this).attr('class'))
			.insertAfter(this);

		});

		$(lines).remove();
	}



	function replaceCirclesWithPaths(parentElement) {

		var circles = $(parentElement).find('circle');

		$.each(circles, function() {

			var cX = $(this).attr('cx');
			var cY = $(this).attr('cy');
			var r = $(this).attr('r');
			var r2 = parseFloat(r * 2);

			var convertedPath = 'M' + cX + ', ' + cY + ' m' + (-r) + ', 0 ' + 'a ' + r + ', ' + r + ' 0 1,0 ' + r2 + ',0 ' + 'a ' + r + ', ' + r + ' 0 1,0 ' + (-r2) + ',0 ';

			$(SVG('path'))
			.attr('d', convertedPath)
			.attr('fill', $(this).attr('fill'))
			.attr('stroke', $(this).attr('stroke'))
			.attr('stroke-width', $(this).attr('stroke-width'))
			.attr('class', $(this).attr('class'))
			.insertAfter(this);

		});

		$(circles).remove();
	}



	function replaceEllipsesWithPaths(parentElement) {


		var ellipses = $(parentElement).find('ellipse');

		$.each(ellipses, function() {

			var cX = $(this).attr('cx');
			var cY = $(this).attr('cy');
			var rX = $(this).attr('rx');
			var rY = $(this).attr('ry');

			var convertedPath = 'M' + cX + ', ' + cY + ' m' + (-rX) + ', 0 ' + 'a ' + rX + ', ' + rY + ' 0 1,0 ' + rX*2 + ',0 ' + 'a ' + rX + ', ' + rY + ' 0 1,0 ' + (-rX*2) + ',0 ';

			$(SVG('path'))
			.attr('d', convertedPath)
			.attr('fill', $(this).attr('fill'))
			.attr('stroke', $(this).attr('stroke'))
			.attr('stroke-width', $(this).attr('stroke-width'))
			.attr('class', $(this).attr('class'))
			.insertAfter(this);

		});

		$(ellipses).remove();
	}




	function replacePolygonsWithPaths(parentElement) {


		var polygons = $(parentElement).find('polygon');

		$.each(polygons, function() {

			var points = $(this).attr('points');
			var polyPoints = points.split(/[ ,]+/);
			var endPoint = polyPoints[0] + ', ' + polyPoints[1];

			$(SVG('path'))
			.attr('d', 'M' + points + ' ' + endPoint)
			.attr('fill', $(this).attr('fill'))
			.attr('stroke', $(this).attr('stroke'))
			.attr('stroke-width', $(this).attr('stroke-width'))
			.attr('class', $(this).attr('class'))
			.insertAfter(this);

		});

		$(polygons).remove();
	}




	function replacePolylinesWithPaths(parentElement) {


		var polylines = $(parentElement).find('polyline');

		$.each(polylines, function() {

			var points = $(this).attr('points');

			$(SVG('path'))
			.attr('d', 'M' + points)
			.attr('fill', $(this).attr('fill'))
			.attr('stroke', $(this).attr('stroke'))
			.attr('stroke-width', $(this).attr('stroke-width'))
			.attr('class', $(this).attr('class'))
			.insertAfter(this);

		});

		$(polylines).remove();
	}


	

	// создаем пустой массив и проверяем поддерживается ли indexOf
	function  existInArray(value, array){
		if ([].indexOf) {
		  //var find = function(array, value) {
			return array.indexOf(value);
		  //}
		  //find();
		} else {
		  //var find = function(array, value) {
			for (var i = 0; i < array.length; i++) {
			  if (array[i] === value) return i;
			 
			//}
			//find();
			
			return -1;
		  }
		  
		}
	}


	function drawSVGPaths(_g_array, _timeMin, _timeMax, _timeDelay, _array) {
		
		for (var ii = 0; ii < _g_array.length; ii++) {
				
			var elems = $('#' + _g_array[ii]).get().reverse();
			var array = _array;
					
			$.each( elems, function(i) {
				
				var this_path = $(this).find('path');
								
				var timeMin = 500;
				var timeMax = 5000;
				var timeDelay = 200;
								
				/* 
				var timeMin = _timeMin;
				var timeMax = _timeMax;
				var timeDelay = _timeDelay;
				 */
				
				// Получаем id ближайшего родителя
				var parent_id = $( this ).attr('id');
				//var parent_id = _g_array[i];
				if(parent_id !==undefined && parent_id.slice(0,4) == 'lamp'){
					parent_id = 'lamps';
				}
				
				/* if (parent_id == 'potolok') {
					timeMax = 500;
					timeMin = 5000;
				} */
				var exist = existInArray(parent_id, array);
				
				if (exist !== -1) { 
							
				//animate
				// Тут, кстати, в зависимости от id мы можем менять тайминги (поменьше для многочисленных элементов), задавать очереди .queue(n) и др.
				// для того, чтобы ускорить сам процесс отрисовки и распараллеливания анимаций. Например: ряд линий в жалюзи можно рисовать одновременно
				

					this_path.delay(timeDelay*i).animate({
						'stroke-dashoffset': 0
					}, {
						duration: Math.floor(Math.random() * timeMax) + timeMin
						,easing: 'easeInOutQuad'
						,complete: function(){
							//_parentElement.toggleClass('on');
							
							
							if (parent_id == 'smallroom-window-left') {
								// Если это последний элемент, то запускаем функцию показа фото
								showPhoto();
							}
						
						
						
							$(this).css({
								'stroke': '#898989'
							});
							
							
						}
						
						
					});
				}
				
			});
		}
	}


	function getLenghtSVGPaths(_g_array) {
		var elems = $('#' + _g_array).find('path').get().reverse();
		
		$.each( elems, function(i) {
			
			var totalLength = this.getTotalLength();
			$(this).css({
				'stroke-dashoffset': totalLength,
				'stroke-dasharray': totalLength + ' ' + totalLength,
				'stroke': '#2C88B7'
			});
					
		});
	}

	function replaceWithPaths(parentElement) {
		replaceRectsWithPaths(parentElement);
		replaceLinesWithPaths(parentElement);
		replaceEllipsesWithPaths(parentElement);
		replaceCirclesWithPaths(parentElement);
		replacePolygonsWithPaths(parentElement);
		replacePolylinesWithPaths(parentElement);    
	}

	
	function showPhoto() {
		
		var img_div = $('#img'),
			 svg_div = $('#svg'),
			 img = img_div.find('img.room'),
			 front_content = $('#front_content'),
			 container = $('#container'),
			 points_wrapper = $('#points_wrapper'),
			 preloader = $('#preloader'),
			 front_slider = $("#front_slider");
			
			// preloader.hide();
		img.stop().animate({
				opacity: 1
			}, 1000, function(){
					svg_div.stop().animate({
					opacity: 0
				}, 700, function(){
					img.addClass('blur');
					//battery.css({'opacity': 1}).addClass('bat-animation');
					front_content.css({'opacity': 1});
					
					setTimeout(function(){
						img.css({'opacity': 0});
						svg_div.css({
							'opacity': 0});
					}, 1000);
					setTimeout(function(){
						img.remove();
						svg_div.remove();
						// $("#content").remove();
						// $("#loader").remove();
						container.show();
						
						front_slider.show();
						
						if ( Detector.webgl ) {
							points_wrapper.show();
						}
						
					}, 3000);
					
					
					
				});
			}
		);
	}


	
	function startSVGAnimation(parentElement, array_id_1, array_id_2, array_id_3) {
		drawSVGPaths(parentElement, 500, 1000, 50, array_id_1);
		drawSVGPaths(parentElement, 500, 1000, 50, array_id_2);
		drawSVGPaths(parentElement, 500, 1000, 50, array_id_3);
		
	}
	
	
	  
	  
	  

			
	})
})(jQuery);