(function ($) {

    $( document ).ready(function(){

        var map;

        var myCenter=new google.maps.LatLng(50.399956, 30.523356);
        var myPosition1=new google.maps.LatLng(50.399956, 30.523356);


        var MY_MAPTYPE_ID = 'custom_style';


        function initialize() {

            var featureOpts =[{"featureType":"all","stylers":[{"saturation":-100},{"gamma":0.5}]}];



            var mapProp = {
                center:myCenter,
                zoom:17,
                scrollwheel: false,
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID, MY_MAPTYPE_ID]
                    //mapTypeIds: [MY_MAPTYPE_ID]
                },
                //mapTypeId: MY_MAPTYPE_ID
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };


            //var objectForm=[object1, object2, object3, object4, object5, object6, object1];
            /* var objectPath=new google.maps.Polygon({
             path:objectForm,
             strokeColor:"#48b71c",
             strokeOpacity:0.8,
             strokeWeight:2,
             fillColor:"#48b71c",
             fillOpacity:0.3
             }); */

            var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            var styledMapOptions = {
                name: 'GrayScale'
            };

            var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
            map.mapTypes.set(MY_MAPTYPE_ID, customMapType);


            var marker1=new google.maps.Marker({
                position:myPosition1,
                icon:'/sites/all/themes/optimax/images/map-icon.png',
                //label: 'O',
            });
            /* var marker2=new google.maps.Marker({
             position:object_center,
             //icon:'/sites/all/themes/tangoargentino/img/map_icon.png',
             label: 'E',
             }); */

            /*  Info Windows  */

            var infoCont1 = '<div class="map-info" style="min-height: 60px; min-width: 240px; text-align: left; "><div><strong>Компания Optimax:</strong><br />Киев, ул. Голосеевская 9, оф. 1</div></div>';

            //var infoCont2 = '<div class="map-info" style="min-height: 60px; min-width: 240px;"><div><strong>Еко-поліс Оксамит</strong></div><div>м. Бровари, Київська обл.</div></div>';

            var infowindow1 = new google.maps.InfoWindow({
                content: infoCont1
            });
            /* var infowindow2 = new google.maps.InfoWindow({
             content: infoCont2
             }); */

            google.maps.event.addListener(marker1, 'click', function() {
                infowindow1.open(map,marker1);
            });
            // google.maps.event.addListener(marker2, 'click', function() {
            // infowindow2.open(map,marker2);
            // });

            /* google.maps.event.addListener(objectPath, 'click', function() {
             infowindow2.open(map,marker2);
             }); */

            /*  Set markers   */
            marker1.setMap(map);
            //marker2.setMap(map);

            //objectPath.setMap(map);



        }

        google.maps.event.addDomListener(window, 'load', initialize);


    });
})(jQuery);