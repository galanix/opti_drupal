(function($) {
    $(window).on('load', function(){
		
		var map_wrapper = $("#svg_hed");
		var map_sidebar =  $('#map_sidebar');
		var dealer_preloader =  $('#dealer_preloader');
		var dealer_close =  $('#dealer_close');
		var dealer_city_wrapper =  $("#dealer_city_wrapper");
		
		var region_name_div =  $("#region_name_div");
		
		//dealer_preloader.hide();
		
        function svgAnimate() {
            if ($('#svg_hed').length > 0) {
                var element 	= document.getElementById('svg_hed_2')
                    ,	docContent 	= element.contentDocument	//get the inner DOM of alpha.svg
                    ,	svgRoot  	= docContent.documentElement
                    ,	svg 		= docContent.getElementById('map_ua')
                    ,	snapSvg 	= Snap(svg)
                    ,	poly 		= snapSvg.selectAll('.jvectormap-region'); 


                poly.forEach( function( myPath ) {

                    // var path = myPath.attr('id');
                    // console.log(region_button);
                    // myPath.attr("value", region_button[path].name);


                    myPath.mouseover(function(ee) {
                        this.animate({
                            fill: "#0A4B81"
                        }, 200);
						
						var selected_region = this;
						var id = selected_region.attr("id");
						var region_info = getRegionInfo();
						var region_name = region_info[id].name;
						
						region_name_div.show().html(region_name);
						
						selected_region.mousemove(function(event) {
							region_name_div.css({
								"top": event.clientY+18,
								"left": event.clientX+12
							});
							// console.log(event);
						 });
				    });
				   
                    myPath.mouseout(function() {
                        this.animate({
                            fill: "#1076C8"
                        }, 200);
						region_name_div.html("").hide();
                    });
					
                    myPath.click(function() {
                     
						//alert("boom!");
						
						var region_name_span = $("#region_name");
						
						var selected_region = this;
						var id = selected_region.attr("id");
						var region_info = getRegionInfo();
						var dr_id = region_info[id].dr_id;
						var region_name = region_info[id].name;


						/*  region_dealer_marckup = 
										'<div class="dealer-region-block">'+
											'<div class="dealer-region"><span># </span>' + region_name + '</div>'+
											'<div class="dealer-city-wrapper">'+
											'</div>'+
										  '</div>'; */

						
						region_name_span.html(region_name);
			
                        if(map_wrapper.hasClass("active")) {
							
						//console.log(selected_region);
						
							if (selected_region.hasClass("active-fill")) {
								
									 map_wrapper.removeClass("active");
									 
									 map_sidebar.animate({
										opacity: 0
									}, 300, function() {
										//map_sidebar.css( "display", "none" );
										map_sidebar.hide();
										dealer_city_wrapper.html("");
										 map_wrapper.animate({
											"padding-right": 0
										}, 200);
									});
									
									
									
									//selected_region.removeClass("active-fill");
									snapSvg.selectAll('.jvectormap-region').forEach( function( path_elem ) {
										path_elem.removeClass("active-fill");
									});
									
									
									
									
								   
									selected_region.animate({
										fill: "#1076C8"
									}, 200);
								
								}
								else {
									
									snapSvg.selectAll('.jvectormap-region').forEach( function( path_elem ) {
										path_elem.removeClass("active-fill");
									});
																	
									selected_region.addClass("active-fill");
									
									
									//map_sidebar.find(".dealer-region-block").remove();
									//map_sidebar.append(region_dealer_marckup);									
									//console.log(dr_id);
									//setTimeout(function(){
										dealerRegion(id);
									//}, 100);
									
									poly.animate({
										fill: "#1076C8"
									}, 200);
									selected_region.animate({
										fill: "#FF9900"
									}, 200);
									
									$(".dealer-email").replaceWith(";","\n");
									
								}
							
							
                           
                        }
						
                        else{
                            map_wrapper.addClass("active");
							
                            map_wrapper.animate({
                                "padding-right": 340
                            }, 200, function(){
								//map_sidebar.css( "display", "inline-block" );
								map_sidebar.show();
								map_sidebar.animate({
									opacity: 1
								}, 300);
							});
							
							
							
                            selected_region.addClass("active-fill");
                            //var id = selected_region.attr("id");
							
                            setTimeout(function(){
								
                                //map_sidebar.css( "display", "inline-block" );
								
								//map_sidebar.find(".dealer-region-block").remove();
								//map_sidebar.append(region_dealer_marckup);
								
                                dealerRegion(id);
                           }, 100);
							
                            poly.animate({
                                fill: "#1076C8"
                            }, 200);
                            selected_region.animate({
                                fill: "#FF9900"
                            }, 200);
                            $(".dealer-email").replaceWith(";","\n");
                        }

                       
						
                    });
                });
            }
			dealer_close.on('click', function(){
				map_sidebar.animate({
						opacity: 0
					}, 300, function() {
						//map_sidebar.css( "display", "none" );
						map_sidebar.hide();
						dealer_city_wrapper.html("");
						 map_wrapper.animate({
							"padding-right": 0
						}, 200);
				});
				
				map_wrapper.removeClass("active");
				snapSvg.selectAll('.jvectormap-region').forEach( function( path_elem ) {
					path_elem.removeClass("active-fill");
				});
			});
		
        }
		
		svgAnimate();





		function getDealerMarckup (arr, cssclass, span) {
			var span = span || '';
			var output = '';
			if (arr != '') {
				if (span != '') {
					output = '<div class="dealer-info-inner dealer-' + cssclass + '"><span>' + span + ': </span>' + arr + '</div>';

				}
				else {
					output = '<div class="dealer-info-inner dealer-' + cssclass + '">' + arr + '</div>';

				}
				
			}
			return output;
		}
		
		
		
		
        function dealerRegion(rnum) {
			var region_info = getRegionInfo();
			var dr_id = region_info[rnum].dr_id;
			var region_name = region_info[rnum].name;
			
			/*  region_dealer_marckup = 
                            '<div class="dealer-region-block">'+
								'<div class="dealer-region"><span># </span>' + region_name + '</div>'+
								'<div class="dealer-city-wrapper">'+
								'</div>'+
							  '</div>';
							  
			map_sidebar.find(".dealer-region-block").remove();
			map_sidebar.append(region_dealer_marckup); */
			
            $.ajax({
                type: 'GET',
                url: 'http://optimax.progt.org/dealer-json/'+ dr_id,
                async: true,
                jsonCallback: 'jsonCallback',
                contentType: 'application/json',
                dataType: 'json',
                beforeSend: function(){
					// console.log("before");
					dealer_city_wrapper.hide();
					dealer_preloader.show();
                },
                success: function(json) {
                    //console.log(json);
					var city_dealer_markup = '';
					dealer_city_wrapper.html("");
					if (json["nodes"].length == 0) {
						city_dealer_markup += 
							'<div class="dealer-city-block">'+
								'<div class="dealer-city"></div>'+
								'<div class="dealer-info"><div class="no_dealer_text">Дилеров в этой области пока нет.</div><a href="/contacts" target="_blank" class="wanna_be_dealer">Хочу стать дилером!</a></div>'+
							'</div>';
					}
					else {
						 $.each(json["nodes"], function(i, node) {
							city_dealer_markup += 
							'<div class="dealer-city-block">'+
								'<div class="dealer-city"><span>г. </span>' + json.nodes[i].node.city + '</div>'+
								'<div class="dealer-info">' +
									getDealerMarckup(json.nodes[i].node.title, "title") +
									getDealerMarckup(json.nodes[i].node.phone, "phone") +
									getDealerMarckup(json.nodes[i].node.email, "email") +
									getDealerMarckup(json.nodes[i].node.web, "web") +
									getDealerMarckup(json.nodes[i].node.contactos, "contact", "Конт. лицо") +
									getDealerMarckup(json.nodes[i].node.dtype, "type", "Тип") +
									getDealerMarckup(json.nodes[i].node.prim, "prim", "Прим") +
								'</div>'+
							'</div>';
						});
					}
					
					dealer_city_wrapper.html(city_dealer_markup).show();
					$('.dealer-info').find(".dealer-info-inner").each(function(){
						var rep_text = $(this).text();
						var after_rep_text = rep_text.replace(/;/gi,'\n');
						$(this).text(after_rep_text);
					});
					
                },
                error: function(e) {
                    console.log(e.message);
                },
                complete:function(){
					// console.log("complete");
					dealer_preloader.hide();
                }
            });
        };
		
		function getRegionInfo() {
				var r_info = {
					"jqvmap1_1" : {
						"dr_id" : 10,
						"name" : "Винницкая область"
					},
					"jqvmap1_2" : {
						"dr_id" : 11,
						"name" : "Волынская область"
					},            
					"jqvmap1_3" : {
						"dr_id" : 12,
						"name" : "Днепропетровская область"
					},            
					"jqvmap1_4" : {
						"dr_id" : 13,
						"name" : "Донецкая область"
					},            
					"jqvmap1_5" : {
						"dr_id" : 14,
						"name" : "Житомирская область"
					},            
					"jqvmap1_6" : {
						"dr_id" : 5,
						"name" : "Закарпатская область"
					},            
					"jqvmap1_7" : {
						"dr_id" : 15,
						"name" : "Запорожская область"
					},            
					"jqvmap1_8" : {
						"dr_id" : 16,
						"name" : "Ивано-Франковская область"
					},            
					"jqvmap1_9" : {
						"dr_id" : 4,
						"name" : "Киевская область"
					},            
					"jqvmap1_10" : {
						"dr_id" : 17,
						"name" : "Кировоградская область"
					},            
					"jqvmap1_11" : {
						"dr_id" : 18,
						"name" : "Луганская область"
					},            
					"jqvmap1_12" : {
						"dr_id" : 19,
						"name" : "Львовская область"
					},            
					"jqvmap1_13" : {
						"dr_id" : 20,
						"name" : "Николаевская область"
					},           
					"jqvmap1_14" : {
						"dr_id" : 21,
						"name" : "Одесская область"
					},           
					"jqvmap1_15" : {
						"dr_id" : 22,
						"name" : "Полтавская область"
					},           
					"jqvmap1_16" : {
						"dr_id" : 23,
						"name" : "Ровенская область"
					},           
					"jqvmap1_17" : {
						"dr_id" : 24,
						"name" : "Сумская область"
					},           
					"jqvmap1_18" : {
						"dr_id" : 25,
						"name" : "Тернопольская область"
					},           
					"jqvmap1_19" : {
						"dr_id" : 26,
						"name" : "Харьковская область"
					},           
					"jqvmap1_20" : {
						"dr_id" : 27,
						"name" : "Херсонская область"
					},           
					"jqvmap1_21" : {
						"dr_id" : 28,
						"name" : "Хмельницкая область"
					},           
					"jqvmap1_22" : {
						"dr_id" : 29,
						"name" : "Черкасская область"
					},           
					"jqvmap1_23" : {
						"dr_id" : 30,
						"name" : "Черниговская область"
					},           
					"jqvmap1_24" : {
						"dr_id" : 31,
						"name" : "Черновицкая область"
					},           
					"jqvmap1_25" : {
						"dr_id" : 9,
						"name" : "АР Крым"
					}
				};
				return r_info;
			}
        
    });
}) (jQuery);