(function($) {
    $(document).ready(function() {
    
        //******ScrollingUp******

        var scrollBtn = $('#scrollTop');
        scrollBtn.click(function() {
            $('html,body').animate({scrollTop: 0}, 500);
        });

        $(window).on('scroll', function() {
            ($(this).scrollTop() > 300) ? scrollBtn.fadeIn(600) : scrollBtn.hide();
        });



        var ww = document.body.clientWidth;
        if (ww >=992) {
            $('#nav_inner ul.menu > li.expanded').hover(function() {
                    $(this).find(' > a').addClass("hover-active");

                    $(this).find(' > ul').addClass("block-active");
                },
                function() {$(this).find(' > a').removeClass("hover-active");

                    $(this).find(' > ul').removeClass("block-active");
                });
        }




        $('#nav_inner ul.menu > li ul.menu li a.active').parent().parent().parent().find(' > a').addClass("hover-active-2");
        $('#nav_inner ul.menu > li ul.menu li a.active').parent().parent().parent().find(' > ul').addClass("block-active-2");
        $('#nav_inner ul.menu > li ul.menu li a.active').parent().addClass("hover-active-3");

        // $('#header').click(function() {
        //     $(this).css('background', 'red');
        // });
        // $('#cart').click(function(){
        //     $(this).addClass('active');
        // });



        // var search_form = $('#block-search-form');
        // $("#search").click(function() {
        //     console.log($(this));
        //     if ($(this).hasClass("active")) {
        //         $(this).removeClass("active");
        //         search_form.hide();
        //     }
        //     else {
        //         $(this).addClass("active");
        //         search_form.show();
        //     }
        //     return false;
        // });

        // var cart_block = $('#block-commerce-cart-cart');
        // $("#cart").click(function() {
        //     if ($(this).hasClass("active")) {
        //         $(this).removeClass("active");
        //         cart_block.hide();
        //     }
        //     else {
        //         $(this).addClass("active");
        //         cart_block.show();
        //     }
        //     return false;
        // });

        function showingMenu (event, menuIcon, menuItem, menuSubItem) {
            //var click = $(event.target.id);
                if(event.target.id == menuIcon && event.target.className.indexOf('active') == -1){     
                    $('#' + menuItem).slideDown();
                    $('#' + menuIcon).addClass('active');
                }
                else if(event.target.id == menuIcon && event.target.className.indexOf('active') != -1) {
                    $('#' + menuItem).slideUp();
                    $('#' + menuIcon).removeClass('active');
                }
                else if(event.target.id == menuItem || event.target.id == menuSubItem) {            
                    return;
                }                
                else {
                    $('#' + menuItem).slideUp();
                    $('#' + menuIcon).removeClass('active');
                }
        }
        $('body').click(function(event){showingMenu(event, 'search', 'block-search-form', 'edit-search-block-form--2')} );
        $('body').click(function(event){showingMenu(event, 'cart', 'block-commerce-cart-cart')} );



        /*
        function scrollpopup(e){
            e = e || window.event;
            var popup_o = $(window).scrollTop();
            order_form.css({
                "top":popup_o + 50
            })
        } */
        var order_form = $("#order_form");
        var site_overlay = $("#site_overlay");
        var callback = $('#callback');
        var ZVONOK_OVERLAY_OUT_TIME = 700;
        var ZVONOK_OVERLAY_IN_TIME = 550;
        var order_close = $('#order_close');
        callback.click(function(){
            callback.addClass("active");
                site_overlay.show().stop().animate({
                    opacity: 0.7,
                }, ZVONOK_OVERLAY_IN_TIME);
                setTimeout(function() {
                    order_form.show();
                }, ZVONOK_OVERLAY_IN_TIME + 100);

        });
       /* callback.click(scrollpopup); */
        order_close.click(function(){
            order_form.hide();
            callback.removeClass("active");
            setTimeout(function() {
                site_overlay.stop().animate({
                    opacity: 0,
                }, ZVONOK_OVERLAY_OUT_TIME);
            }, 100);
            setTimeout(function() {
                site_overlay.hide();
            }, ZVONOK_OVERLAY_OUT_TIME + 200);
            return false;
        });
        site_overlay.click(function(){
            order_form.hide();
            callback.removeClass("active");
            setTimeout(function() {
                site_overlay.stop().animate({
                    opacity: 0,
                }, ZVONOK_OVERLAY_OUT_TIME);
            }, 100);
            setTimeout(function() {
                site_overlay.hide();
            }, ZVONOK_OVERLAY_OUT_TIME + 200);
            return false;
        });

        //-----------------------------------------------------------------------------------------
        // Добавляем маску для телефона
        //-----------------------------------------------------------------------------------------
        $("#edit-submitted-vash-nomer-telefona").mask("999 999-99-99");
        $("#edit-customer-profile-billing-field-order-phone-und-0-value").mask("999 999-99-99");
            
        $(function(){
            $('.field-collection-view').each(function(){

                if ( $(this).find('.field-name-field-about-us-img').length == 0 ) {
                    $(this).find(".group-about-us-wrapper").addClass("full-width");
                }

            });
        });


        //-----------------------------------------------------------------------------------------
        // Добавляем кнопку закрыть для системных сообщений
        //-----------------------------------------------------------------------------------------
        var messages = $(".messages");
        messages.append('<span class="close_message"><i class="fa fa-times" aria-hidden="true"></i></span>');
        var close_message = $(".close_message");

        close_message.click(function() {
            $(this).parent().slideUp();
            return false;
        });

        //-----------------------------------------------------------------------------------------
        // Добавляем кнопку закрыть для табов админа
        //-----------------------------------------------------------------------------------------
        var messages = $(".tabs-wrapper");
        messages.append('<span class="close_message"><i class="fa fa-times" aria-hidden="true"></i></span>');
        var close_message = $(".close_message");

        close_message.click(function() {
            $(this).parent().slideUp();
            return false;
        });

        $(".breadcrumb").find(".br_separator").html("/");
        if (ww <=700) {
            $("#footer_container > .region .block-bean").click(function () {
                if ($(this).hasClass("active")) {
                    $(this).removeClass("active");
                    $(this).find(".field-name-field-footer-link").slideUp();
                }
                else {
                    $("#footer_container > .region .block-bean").removeClass("active");
                    $(this).addClass("active");
                    $("#footer_container > .region .block-bean").find(".field-name-field-footer-link").slideUp();
                    $(this).find(".field-name-field-footer-link").slideDown();
                }
                return false;
            });
        }


        //-------------
        // Мобильная навигация
        //-------------
        var navigation_small_menu = $('#nav_top');
        var navigation = $('#navigation');
        navigation_small_menu.click(function() {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                navigation.slideUp();
            }
            else {
                $(this).addClass("active");
                navigation.slideDown();
            }
            return false;
        });

        $("#callback").append('<p id="callMeBack">Перезвонить мне</p>');
        $(window).bind('load resize orientationchange', function() {
            var ww = document.body.clientWidth;
            if (ww >1024) {
                 navigation.css('display', 'inline-block');
            }
            else {
                if (navigation_small_menu.hasClass("active")) {
                    navigation.css('display', 'inline-block');
                }
                else{
                    navigation.css('display', 'none');
                }
            }

            if(ww <= 780){
                $('.field-name-field-about-us-img').each(function() {
                    $(this).parent().find('.group-about-us-wrapper').before(this);
                });           
            }
            else {
                $('.field-name-field-about-us-img').each(function() {
                    $(this).parent().find('.group-about-us-wrapper').after(this);
                });       
            }

        });



        $('.views-field-field-prod-reference .form-item-quantity input, .views-field-edit-quantity input, .field-name-field-product-reference .form-item-quantity input').each(function(){
            $(this).after('<a href="#" class="quantityChanger quantityPlus" title="Увеличить">+</a>');
            $(this).before('<a href="#" class="quantityChanger quantityMinus" title="Уменьшить">&minus;</a>');

        });

        $('.quantityPlus').click(function(){
            var t = $(this);
            var curNum = parseInt(t.prev().attr('value'));
            t.prev().attr('value', curNum+1);
            return false;
        });

        $('.quantityMinus').click(function(){
            var t = $(this);
            var curNum = parseInt(t.next().attr('value'));
            if(curNum > 1){
                t.next().attr('value', curNum-1);
            }
            return false;
        });


        $("div.messages.commerce-add-to-cart-confirmation .message-inner .button-wrapper > .button.checkout a").html("Перейти к оформлению");
        $( "body.sidebar-second.node-type-contacts #block-block-1 h2" ).wrapAll( "<div class='tittle_wrap' />");
    	if ($("body").hasClass("page-node-2")) {
			var new_crubms = $(".breadcrumb");
			$(".group-slider-wrapper").append(new_crubms);
		}
        if ($("body").hasClass("page-node-26")) {
            var new_buttuns = $("#main").find(".title-drop");
            $("#main > footer").append(new_buttuns);
        }
        $(".field-name-field-product-gphoto .field-items").slick({
                dots: true,
                infinite: true,
                arrows: false,
                autoplay: false,
                autoplaySpeed: 5000,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 2,
                responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true
                  }
                },
                {
                  breakpoint: 980,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                }//,
                //{
                //  breakpoint: 660,
                //  settings: {
                //    slidesToShow: 1,
                //    slidesToScroll: 1
                //  }
                //}
                ]
		});
            $('.view-related-radiators').find('.view-content').slick({
                infinite: true,
                autoplay: false,
                dots: true,
				arrows: false,
                autoplaySpeed: 5000,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 2,
                responsive: [
                    {breakpoint: 1024,settings: {slidesToShow: 3,slidesToScroll: 2}},
                    {breakpoint: 800,settings: {slidesToShow: 3,slidesToScroll: 1}},
                    {breakpoint: 768,settings: {slidesToShow: 2,slidesToScroll: 1}},
                    {breakpoint: 480,settings: {slidesToShow: 1,slidesToScroll: 1}}//,
                    //{breakpoint: 380,settings: {slidesToShow: 1,slidesToScroll: 1}}
                ]
            });
        $('.field-name-field-related-articles').find('.view-id-related_articles >  .view-content').slick({
            infinite: true,
            autoplay: false,
            dots: true,
			arrows: false,
            autoplaySpeed: 5000,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 2,
            responsive: [
                {breakpoint: 1024,settings: {slidesToShow: 3,slidesToScroll: 2}},
                {breakpoint: 800,settings: {slidesToShow: 3,slidesToScroll: 1}},
                {breakpoint: 600,settings: {slidesToShow: 2,slidesToScroll: 1}},
                {breakpoint: 480,settings: {slidesToShow: 2,slidesToScroll: 1}},
                {breakpoint: 380,settings: {slidesToShow: 1,slidesToScroll: 1}}
            ]
        });
        $('.field-name-field-logo-item').find('.field-items').slick({
            infinite: true,
            autoplay: true,
            dots: true,
            arrows: false,
            autoplaySpeed: 5000,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {breakpoint: 1024,settings: {slidesToShow: 3,slidesToScroll: 2}},
                {breakpoint: 800,settings: {slidesToShow: 3,slidesToScroll: 1}},
                {breakpoint: 600,settings: {slidesToShow: 2,slidesToScroll: 1}},
                {breakpoint: 480,settings: {slidesToShow: 2,slidesToScroll: 1}},
                {breakpoint: 380,settings: {slidesToShow: 1,slidesToScroll: 1}}
            ]
        });
        var url = window.location.href;
        $("#sidebar-second .views-row .field-content a").each(function() {
            var value = "http://optimax.progt.org" + $(this).attr('href');
            if(value==url){
                $(this).parent().addClass('active-trail');
                $(this).addClass('active');
            }
        });
        $('.field-name-field-product-related .productview .views-row').each(function(){
            $(this).find("> div").wrapAll($('<div class="wrapp-container"></div>'));
        });
        $('#main > .productview .views-row').each(function(){
            $(this).find("> div").wrapAll($('<div class="wrapp-container"></div>'));
        });

        //******Depended Fields in Order Form******
        var defualRadioButton = $('#edit-customer-profile-billing-field-cust-shipping-und-sam-vyvoz').attr('checked',true);
        var allContactInputs = $('.field-name-field-order-city, .field-name-field-order-serv-name, .field-name-field-order-serv-number, .field-name-field-order-address, .field-name-field-order-comment');
        function buildingDependencies() {
            var radioInput = $(this).val();
              if(radioInput  == 'sam_vyvoz' ) {
                    allContactInputs.hide();
                    $('.field-name-field-order-comment').fadeIn(1000);  
                }
            else if(radioInput == 'np' || radioInput == 'delivery' || radioInput == 'intime' ) {
                allContactInputs.hide();
                $('.field-name-field-order-city, #edit-customer-profile-billing-field-order-serv-number').fadeIn(1000);
            }
            else if(radioInput == 'other_method' ) {
                 allContactInputs.hide();
                $('.field-name-field-order-city, .field-name-field-order-serv-name, .field-name-field-order-serv-number').fadeIn(1000);
            }
            else if(radioInput == 'kuryer') {
                 allContactInputs.hide();
               $('.field-name-field-order-address').fadeIn(1000);
            }          
        }
        $('.form-radio').click(buildingDependencies);
        $("#second_menu").click(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this).parent().find("#block-menu-menu-company-side-menu").removeClass("active");
                $(this).parent().find("#block-menu-menu-contacts-side-menu").removeClass("active");
                $(this).parent().find("#block-views-radiator-side-block-block").removeClass("active");
            }
            else {
                $(this).addClass("active");
                $(this).parent().find("#block-menu-menu-company-side-menu").addClass("active");
                $(this).parent().find("#block-menu-menu-contacts-side-menu").addClass("active");
                $(this).parent().find("#block-views-radiator-side-block-block").addClass("active");
            }
            return false;
        });

        //-------------------------------
        // Video
        //-------------------------------
        var youtube = document.querySelectorAll( ".youtube" );
        for (var i = 0; i < youtube.length; i++) {
            var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
            var image = new Image();
            image.src = source;
            image.addEventListener( "load", function() {
                youtube[ i ].appendChild( image );
            }( i ) );
            youtube[i].addEventListener( "click", function() {
                var iframe = document.createElement( "iframe" );
                iframe.setAttribute( "frameborder", "0" );
                iframe.setAttribute( "allowfullscreen", "" );
                iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );
                this.innerHTML = "";
                this.appendChild( iframe );
            } );
        };













        var svg_div = $('.field-name-field-svg-image');
        svg_div.css({
            opacity: 0
        });

        $(window).on('load', function(){
            svg_div.css({
                opacity: 1
            });

            jQuery.extend( jQuery.easing,
                {
                    easeInOutQuad: function (x, t, b, c, d) {
                        if ((t/=d/2) < 1) return c/2*t*t + b;
                        return -c/2 * ((--t)*(t-2) - 1) + b;
                    }
                });

            function SVG(tag) {
                return document.createElementNS('http://www.w3.org/2000/svg', tag);
            }
            function replaceRectsWithPaths(parentElement) {
                var rects = $(parentElement).find('rect');
                $.each(rects, function() {

                    var rectX = $(this).attr('x');
                    var rectY = $(this).attr('y');

                    var rectX2 = parseFloat(rectX) + parseFloat($(this).attr('width'));
                    var rectY2 = parseFloat(rectY) + parseFloat($(this).attr('height'));

                    var convertedPath = 'M' + rectX + ',' + rectY + ' ' + rectX2 + ',' + rectY + ' ' + rectX2 + ',' + rectY2 + ' ' + rectX + ',' + rectY2 + ' ' + rectX + ',' + rectY;


                    $(SVG('path'))
                        .attr('d', convertedPath)
                        .attr('fill', $(this).attr('fill'))
                        .attr('stroke', $(this).attr('stroke'))
                        .attr('stroke-width', $(this).attr('stroke-width'))
                        .attr('class', $(this).attr('class'))
                        .insertAfter(this);

                });

                $(rects).remove();
            }
            function replaceLinesWithPaths(parentElement) {


                var lines = $(parentElement).find('line');

                $.each(lines, function() {

                    var lineX1 = $(this).attr('x1');
                    var lineY1 = $(this).attr('y1');

                    var lineX2 = $(this).attr('x2');
                    var lineY2 = $(this).attr('y2');

                    var convertedPath = 'M' + lineX1 + ',' + lineY1 + ' ' + lineX2 + ',' + lineY2;


                    $(SVG('path'))
                        .attr('d', convertedPath)
                        .attr('fill', $(this).attr('fill'))
                        .attr('stroke', $(this).attr('stroke'))
                        .attr('stroke-width', $(this).attr('stroke-width'))
                        .attr('class', $(this).attr('class'))
                        .insertAfter(this);

                });

                $(lines).remove();
            }
            function replaceCirclesWithPaths(parentElement) {

                var circles = $(parentElement).find('circle');

                $.each(circles, function() {

                    var cX = $(this).attr('cx');
                    var cY = $(this).attr('cy');
                    var r = $(this).attr('r');
                    var r2 = parseFloat(r * 2);

                    var convertedPath = 'M' + cX + ', ' + cY + ' m' + (-r) + ', 0 ' + 'a ' + r + ', ' + r + ' 0 1,0 ' + r2 + ',0 ' + 'a ' + r + ', ' + r + ' 0 1,0 ' + (-r2) + ',0 ';

                    $(SVG('path'))
                        .attr('d', convertedPath)
                        .attr('fill', $(this).attr('fill'))
                        .attr('stroke', $(this).attr('stroke'))
                        .attr('stroke-width', $(this).attr('stroke-width'))
                        .attr('class', $(this).attr('class'))
                        .insertAfter(this);

                });

                $(circles).remove();
            }
            function replaceEllipsesWithPaths(parentElement) {


                var ellipses = $(parentElement).find('ellipse');

                $.each(ellipses, function() {

                    var cX = $(this).attr('cx');
                    var cY = $(this).attr('cy');
                    var rX = $(this).attr('rx');
                    var rY = $(this).attr('ry');

                    var convertedPath = 'M' + cX + ', ' + cY + ' m' + (-rX) + ', 0 ' + 'a ' + rX + ', ' + rY + ' 0 1,0 ' + rX*2 + ',0 ' + 'a ' + rX + ', ' + rY + ' 0 1,0 ' + (-rX*2) + ',0 ';

                    $(SVG('path'))
                        .attr('d', convertedPath)
                        .attr('fill', $(this).attr('fill'))
                        .attr('stroke', $(this).attr('stroke'))
                        .attr('stroke-width', $(this).attr('stroke-width'))
                        .attr('class', $(this).attr('class'))
                        .insertAfter(this);

                });

                $(ellipses).remove();
            }
            function replacePolygonsWithPaths(parentElement) {


                var polygons = $(parentElement).find('polygon');

                $.each(polygons, function() {

                    var points = $(this).attr('points');
                    var polyPoints = points.split(/[ ,]+/);
                    var endPoint = polyPoints[0] + ', ' + polyPoints[1];

                    $(SVG('path'))
                        .attr('d', 'M' + points + ' ' + endPoint)
                        .attr('fill', $(this).attr('fill'))
                        .attr('stroke', $(this).attr('stroke'))
                        .attr('stroke-width', $(this).attr('stroke-width'))
                        .attr('class', $(this).attr('class'))
                        .insertAfter(this);

                });

                $(polygons).remove();
            }
            function replacePolylinesWithPaths(parentElement) {


                var polylines = $(parentElement).find('polyline');

                $.each(polylines, function() {

                    var points = $(this).attr('points');

                    $(SVG('path'))
                        .attr('d', 'M' + points)
                        .attr('fill', $(this).attr('fill'))
                        .attr('stroke', $(this).attr('stroke'))
                        .attr('stroke-width', $(this).attr('stroke-width'))
                        .attr('class', $(this).attr('class'))
                        .insertAfter(this);

                });

                $(polylines).remove();
            }

            function drawSVGPaths(_parentElement, _timeMin, _timeMax, _timeDelay) {
                var elems = $(_parentElement).find('path').get().reverse();

                //for each PATH..
                $.each( elems, function(i) {


                    var timeMin = _timeMin;
                    var timeMax = _timeMax;
                    var timeDelay = _timeDelay;


                    var totalLength = this.getTotalLength();


                    //set PATHs to invisible
                    $(this).css({
                        'stroke-dashoffset': totalLength,
                        'stroke-dasharray': totalLength + ' ' + totalLength,
                        'stroke': '#2C88B7'
                    });




                    $(this).delay(timeDelay*i).animate({
                        'stroke-dashoffset': 0
                    }, {
                        duration: Math.floor(Math.random() * timeMax) + timeMin
                        ,easing: 'easeInOutQuad'
                        ,complete: function(){
                            var element_id = $( this).parent().attr('id');
                            console.log(element_id);
                            if (element_id !== 'mechanik'){
                                $(this).css({
                                    'stroke': '#000',
                                    'fill':'#000'
                                });
                            }
                            else{
                                $(this).css({
                                    'stroke': '#000'
                                });
                            }
                        }
                    });
                });
            }

            function replaceWithPaths(parentElement) {
                replaceRectsWithPaths(parentElement);
                replaceLinesWithPaths(parentElement);
                replaceEllipsesWithPaths(parentElement);
                replaceCirclesWithPaths(parentElement);
                replacePolygonsWithPaths(parentElement);
                replacePolylinesWithPaths(parentElement);
            }
            function startSVGAnimation(parentElement) {
                drawSVGPaths(parentElement, 50, 250, 20);
            }


            $(".field-name-field-svg-text > .field-items > .field-item").each(function(){
                var local_svg = $(this).find("object");
                var object_id = local_svg.attr('id');
                var svg_id = object_id.slice(0,-4);
                var element 	= document.getElementById(object_id)
                        ,	docContent 	= element.contentDocument	//get the inner DOM of alpha.svg
                        ,	svgRoot  	= docContent.documentElement
                        ,	svg 		= docContent.getElementById(svg_id);
                replaceWithPaths(svg);
                startSVGAnimation(svg);

            });
        });



	
	
	
	





    });
}) (jQuery);