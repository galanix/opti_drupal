(function ($) {

    $( document ).ready(function(){
		
		var map;

        var myCenter=new google.maps.LatLng( 50.402903538019046, 30.522927045822144 );
        var myPosition1=new google.maps.LatLng(50.399956, 30.523356);


        var MY_MAPTYPE_ID = 'custom_style';


        function initialize() {

            var featureOpts =[{"featureType":"all","stylers":[{"saturation":-100},{"gamma":0.5}]}];



            var mapProp = {
                center:myCenter,
                zoom:16,
				// animatedZoom: false,
                scrollwheel: false,
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID, MY_MAPTYPE_ID]
                    //mapTypeIds: [MY_MAPTYPE_ID]
                },
                //mapTypeId: MY_MAPTYPE_ID
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };


            //var objectForm=[object1, object2, object3, object4, object5, object6, object1];
            /* var objectPath=new google.maps.Polygon({
             path:objectForm,
             strokeColor:"#48b71c",
             strokeOpacity:0.8,
             strokeWeight:2,
             fillColor:"#48b71c",
             fillOpacity:0.3
             }); */

            var map=new google.maps.Map(document.getElementById("google_map_route"),mapProp);
            var styledMapOptions = {
                name: 'GrayScale'
            };

            var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
            map.mapTypes.set(MY_MAPTYPE_ID, customMapType);


            var marker1=new google.maps.Marker({
                position:myPosition1,
                icon:'/sites/all/themes/optimax/images/map-icon.png',
                //label: 'O',
            });
            /* var marker2=new google.maps.Marker({
             position:object_center,
             //icon:'/sites/all/themes/tangoargentino/img/map_icon.png',
             label: 'E',
             }); */

            /*  Info Windows  */

            var infoCont1 = '<div class="map-info" style="min-height: 60px; min-width: 240px; text-align: left; "><div><strong>Компания Optimax:</strong><br />Киев, ул. Голосеевская 9, оф. 1</div></div>';

            //var infoCont2 = '<div class="map-info" style="min-height: 60px; min-width: 240px;"><div><strong>Еко-поліс Оксамит</strong></div><div>м. Бровари, Київська обл.</div></div>';

            var infowindow1 = new google.maps.InfoWindow({
                content: infoCont1
            });
            /* var infowindow2 = new google.maps.InfoWindow({
             content: infoCont2
             }); */

            google.maps.event.addListener(marker1, 'click', function() {
                infowindow1.open(map,marker1);
            });
            // google.maps.event.addListener(marker2, 'click', function() {
            // infowindow2.open(map,marker2);
            // });

            /* google.maps.event.addListener(objectPath, 'click', function() {
             infowindow2.open(map,marker2);
             }); */

            /*  Set markers   */
            marker1.setMap(map);
            //marker2.setMap(map);

            //objectPath.setMap(map);
			
			
			google.maps.event.addListener(map, 'click', function(event) {
				console.log("Click coords.:");
				console.log( event.latLng.lat() + ", " + event.latLng.lng() );
				console.log("Zoom:");
				console.log(map.zoom);
				console.log("Map center:");
				console.log(map.getCenter().lat() + ", " + map.getCenter().lng() );
				console.log("------------------");
            });
			
			
			// https://stackoverflow.com/questions/3817812/google-maps-v3-can-i-ensure-smooth-panning-every-time
			var panPath = [];   // An array of points the current panning action will use
			var panQueue = [];  // An array of subsequent panTo actions to take
			// var STEPS = 50;     // The number of steps that each panTo action will undergo

			function panTo(newLat, newLng, STEPS) {
			  if (panPath.length > 0) {
				// We are already panning...queue this up for next move
				panQueue.push([newLat, newLng]);
			  } else {
				// Lets compute the points we'll use
				panPath.push("LAZY SYNCRONIZED LOCK");  // make length non-zero - 'release' this before calling setTimeout
				var curLat = map.getCenter().lat();
				var curLng = map.getCenter().lng();
				var dLat = (newLat - curLat)/STEPS;
				var dLng = (newLng - curLng)/STEPS;

				for (var i=0; i < STEPS; i++) {
				  panPath.push([curLat + dLat * i, curLng + dLng * i]);
				}
				panPath.push([newLat, newLng]);
				panPath.shift();      // LAZY SYNCRONIZED LOCK
				setTimeout(doPan, 10);
			  }
			}

			function doPan() {
			  var next = panPath.shift();
			  if (next != null) {
				// Continue our current pan action
				map.panTo( new google.maps.LatLng(next[0], next[1]));
				setTimeout(doPan, 10 );
			  } else {
				// We are finished with this pan - check if there are any queue'd up locations to pan to 
				var queued = panQueue.shift();
				if (queued != null) {
				  panTo(queued[0], queued[1]);
				}
			  }
			}
			


			
			
			
			var m_demeevskaya = new google.maps.LatLng( 50.40467806922079, 30.517460703849792 );
			
			var vern1 = new google.maps.LatLng( 50.40401476475589, 30.51705300807953 );
			var vern2 = new google.maps.LatLng( 50.40343693010418, 30.51904857158661 );
			var vern3 = new google.maps.LatLng( 50.40363866016174, 30.519670844078064 );
			var vern4 = new google.maps.LatLng( 50.40347112169981, 30.52030920982361 );
			var vern5 = new google.maps.LatLng( 50.403262552582866, 30.520201921463013 );
			var vern6 = new google.maps.LatLng( 50.403159977270604, 30.521451830863953 );
			var vern7 = new google.maps.LatLng( 50.40199060301953, 30.521188974380493 );
			var vern8 = new google.maps.LatLng( 50.401894863498974, 30.521339178085327 );
			
			var nauka1 = new google.maps.LatLng( 50.40524905068032, 30.517916679382324 );
			var nauka2 = new google.maps.LatLng( 50.405399487764136, 30.51831364631653 );
			var nauka3 = new google.maps.LatLng( 50.405433677943854, 30.519708395004272 );
			var nauka4 = new google.maps.LatLng( 50.40591917583385, 30.52078127861023 );
			var nauka5 = new google.maps.LatLng( 50.40591917583385, 30.521178245544434 );
			var nauka6 = new google.maps.LatLng( 50.4058439581765, 30.52180051803589 );
			var nauka7 = new google.maps.LatLng( 50.403983992673936, 30.52430033683777 );
			var nauka8 = new google.maps.LatLng( 50.4035497622761, 30.523560047149658 );
			
			var gol1 = new google.maps.LatLng( 50.403983992673936, 30.52430033683777 );
			var gol2 = new google.maps.LatLng( 50.4035497622761, 30.523560047149658 );
			var gol3 = new google.maps.LatLng( 50.403252295061634, 30.52300214767456 );
			var gol4 = new google.maps.LatLng( 50.40253768228412, 30.52204728126526 );
			var gol5 = new google.maps.LatLng( 50.40108448909662, 30.520389676094055 );
			var gol6 = new google.maps.LatLng( 50.40092719963233, 30.520684719085693 );
			
			var park1 = new google.maps.LatLng( 50.40092719963233, 30.520684719085693 );
			var park2 = new google.maps.LatLng( 50.40051687683376, 30.521596670150757 );
			var park3 = new google.maps.LatLng( 50.400896425545646, 30.52199900150299 );
			var park4 = new google.maps.LatLng( 50.40046900560922, 30.52274465560913 );
			var park5 = new google.maps.LatLng( 50.40028777839258, 30.522709786891937 );
			
			var sto1 = new google.maps.LatLng( 50.40028777839258, 30.522709786891937 );
			var sto2 = new google.maps.LatLng( 50.40019716452443, 30.52289754152298 );
			var sto3 = new google.maps.LatLng( 50.39999712952294, 30.523216724395752 );
			var sto4 = new google.maps.LatLng( 50.399949257773464, 30.523315966129303 );

			var golparsto1 = new google.maps.LatLng(50.401091327757136, 30.52038162946701);
			var golparsto2 = new google.maps.LatLng(50.400544231797504, 30.521596670150757);
			var golparsto3 = new google.maps.LatLng(50.40088445783986, 30.522001683712006);
			var golparsto4 = new google.maps.LatLng(50.40045874748338, 30.522763431072235);
			var golparsto5 = new google.maps.LatLng(50.400291197780454, 30.52273392677307);
			var golparsto6 = new google.maps.LatLng(50.399952677185745, 30.52327573299408);

			
			
			/*     Дорога от м. Голосеевская    */
			var ag1 = new google.maps.LatLng( 50.397336754703915, 30.509977340698242 );
			var ag2 = new google.maps.LatLng( 50.39721364891282, 30.51056742668152 );
			var ag3 = new google.maps.LatLng( 50.39793860063603, 30.512326955795288 );
			var ag4 = new google.maps.LatLng( 50.39972357601755, 30.518678426742554 );
			var ag5 = new google.maps.LatLng( 50.39988086947605, 30.518914461135864 );
			var ag6 = new google.maps.LatLng( 50.39988086947605, 30.518914461135864 );
			var ag7 = new google.maps.LatLng( 50.40108448909662, 30.520389676094055 );
						
			
			var ap1 = new google.maps.LatLng( 50.40108448909662, 30.520389676094055 );
			var ap2 = new google.maps.LatLng( 50.40092719963233, 30.520684719085693 );
			var ap3 = new google.maps.LatLng( 50.40051687683376, 30.521596670150757 );
			
			
			
			var app1 = new google.maps.LatLng( 50.40061261913782, 30.521403551101685 );
			var app2 = new google.maps.LatLng( 50.40051687683376, 30.521596670150757 );
			var app3 = new google.maps.LatLng( 50.400896425545646, 30.52199900150299 );
			var app4 = new google.maps.LatLng( 50.40046900560922, 30.52274465560913 );
			var app5 = new google.maps.LatLng( 50.40028777839258, 30.522709786891937 );
			
			

			function path (coord, color = "#0395dd", zindex = 99999999 ) {
					var pathName = new google.maps.Polyline({
						path: coord,
						strokeColor: color,
						strokeOpacity: 0.8,
						strokeWeight: 4,
						zIndex: zindex
					});
					return pathName;
			}

			var p1_path = path([ m_demeevskaya, vern1, vern2, vern3, vern4, vern5, vern6, vern7, vern8 ]);
			var p2_path = path([ m_demeevskaya, nauka1, nauka2, nauka3, nauka4, nauka5, nauka6, nauka7, nauka8 ]);
			var p3_path = path([ gol1, gol2, gol3, gol4, gol5, gol6 ]);
			var p4_path = path([ park1, park2, park3, park4, park5 ]);
			var p5_path = path([ sto1, sto2, sto3, sto4 ]);
			
			var a1_path = path([ ag1, ag2, ag3, ag4, ag5, ag6, ag7 ]);
			var a2_path = path([ ap1, ap2, ap3 ]);
			var a3_path = path([ app1, app2, app3, app4, app5 ]);
			//Path from goloseecvskoy to main building
			var a7_path = path([golparsto1, golparsto2, golparsto3, golparsto4, golparsto5, golparsto6]); 
			
			var p_total_path_1 = path([ m_demeevskaya, vern1, vern2, vern3, vern4, vern5, vern6, vern7, vern8 ], "#a0a0a0", "unset");
			var p_total_path_2 = path([ m_demeevskaya, nauka1, nauka2, nauka3, nauka4, nauka5, nauka6, nauka7, nauka8, gol1, gol2, gol3, gol4, gol5, gol6, park1, park2, park3, park4, park5, sto1, sto2, sto3, sto4 ], "#a0a0a0", "unset");
			var a_total_path_1 = path([ ag1, ag2, ag3, ag4, ag5, ag6, ag7 ], "#a0a0a0", "unset");


			function showPlaceCircle(lat, lng, radius) {
				var place_circle = new google.maps.Circle({
					strokeColor: '#0395dd',
					strokeOpacity: 0.5,
					strokeWeight: 1,
					fillColor: '#0395dd',
					fillOpacity: 0.3,
					// map: map,
					center: new google.maps.LatLng(lat, lng),
					radius: radius,
					zIndex: 99999999
				});
				return place_circle;
			}
			var demeevka_circle = showPlaceCircle("50.40464045942209", "30.517533123493195", 20);
			var vern_circle     = showPlaceCircle("50.404161786649446", "30.51878571510315", 50);
			var eco_circle      = showPlaceCircle("50.40414127199399", "30.523635149002075", 30);
			var bc7_circle      = showPlaceCircle("50.40194615255187", "30.521929264068604", 40);
			var gol13_circle    = showPlaceCircle("50.400345907952655", "30.520159006118774", 50);
			var park_circle     = showPlaceCircle("50.400380101778204", "30.52205801010132", 30);
			var autos_circle    = showPlaceCircle("50.399949257773464", "30.52274465560913", 30);
			var main_circle     = showPlaceCircle("50.399918483051835", "30.523377656936646", 20);
			var golosPl_circle	= showPlaceCircle("50.39739830747958", "30.51006317138672", 30);
			var church_circle	= showPlaceCircle("50.397186292026944", "30.511339902877808", 20);
			var kuz_circle		= showPlaceCircle("50.39857463405495", "30.516323447227478", 30);
			var police_circle	= showPlaceCircle("50.39918672267389", "30.51802396774292", 30);
			var avtokzal_circle	= showPlaceCircle("50.40622004526938", "30.52001953125", 30);



			function removeAllPath() {
				var pathes = [p1_path, p2_path, p3_path, p4_path, p5_path, a1_path, a2_path, a3_path,a7_path];				
				for(var i = 0; i < pathes.length; i++) {
					pathes[i].setMap( null );
				}
				/* p_total_path_1.setMap( null );
				p_total_path_2.setMap( null );
				a_total_path_1.setMap( null ); */			
			}

	
			var p1_center = {
				lat: 50.403505313270756,
				lng: 30.522154569625854,
				radius: 30 
			};
			var p2_center = {
				lat: 50.40384029277356,
				lng: 30.523286262565694, 
				radius: 30 
			}; 
			var p3_center = {
				lat: 50.40200760275108,
				lng: 30.52461663823704, 
				radius: 30 
			}; 
			var p4_center = {
				lat: 50.4006757807468,
				lng: 30.522744456344643, 
				radius: 40 
			}; 
			var p5_center = {
				lat: 50.40005003373809,
				lng: 30.52323530059437, 
				radius: 30 
			};
			var a1_center = {
				lat: 50.39773503993272,
				lng: 30.521762767845154, 
				radius: 30 
			};
			var a2_center = {
				lat: 50.4006757807468,
				lng: 30.522744456344643, 
				radius: 40 
			};
			var a3_center = {
				lat: 50.4006757807468,
				lng: 30.522744456344643, 
				radius: 40 
			};
			var a7_center = {
				lat: 50.400274,
				lng: 30.522647, 
				radius: 30 
			};

			function showMap( path, latlng,  zoom = 17 ) {
				removeAllPath();
				path.setMap(map);
				map.setZoom(zoom);
				panTo( latlng );
			}
			$("#p1").on("click", function(){showMap( p1_path, p1_center)});
			$("#p2").on("click", function(){showMap( p2_path, p2_center)});
			$("#p3").on("click", function(){showMap( p3_path, p3_center)});
			$("#p4").on("click", function(){showMap( p4_path, p4_center)});
			$("#p5").on("click", function(){showMap( p5_path, p5_center)});
			$("#a1").on("click", function(){showMap( a1_path, a1_center, 16 )});
			$("#a2").on("click", function(){showMap( a2_path, a2_center)});
			$("#a3").on("click", function(){showMap( a3_path, a3_center)});
			$("#a4").on("click", function(){showMap( p5_path, p5_center)});
			$("#a5").on("click", function(){showMap( p2_path, p2_center)});
			$("#a6").on("click", function(){showMap( p3_path, p3_center)});
			$("#a7").on("click", function(){showMap( a7_path, a7_center)});

			
			var gmr_tabs = $("#gmr_tabs");
			var gmr_content_item = $("#gmr_content > div");
			
			gmr_tabs.find('a').on('click', function(){
				var link = $( this );
				var text = link.attr("data-text");
				var text_div = $("#" + text);
				
				var map_data  = getMapDataFromTabs();
				var map_data_text = map_data[text];
				
				map_data_text.lines();
				
				// console.log(p_total_path_2);
				
				panTo( map_data_text.center_x, map_data_text.center_y, map_data_text.anim_steps );
				map.setZoom(map_data_text.zoom);
				
				if (link.hasClass("active")) {
					return false;
				}
				else {
					gmr_tabs.find('a').removeClass("active");
					gmr_content_item.removeClass("active");
					
					link.addClass("active");
					text_div.addClass("active");
					
					return false;
				}
			});
			
			var sf_popup = $("#sf_popup");
            
            $("#gmr_content").find(".gmr_content_items > div").on("mouseover", function(e){
			
                sf_popup.html("");
                var sf_el = e.target;
                // console.log(sf_el);
                
            function removeAllCirclles() {
                var circles = [demeevka_circle, vern_circle, eco_circle, bc7_circle,
                                gol13_circle, park_circle, autos_circle,
                                main_circle, golosPl_circle, church_circle, kuz_circle, 
                                police_circle, avtokzal_circle];				
                for(var i = 0; i < circles.length; i++) {
                    circles[i].setMap( null );
                }
            }

			if ( sf_el.classList.contains( "sf" ) ) {
				
				var file_name =  $(sf_el).attr("data-photo");
				var img_src = "/sites/all/themes/optimax/route/" + file_name + ".jpg";
				sf_popup.append('<img src="' + img_src + '" />');
				
				// console.log(e);
				
				sf_popup.css({
					display: "block",
					top: e.clientY - 275,
					left: e.clientX - 220,
					opacity: 1
				});
				
				// Вынести в отдельную функцию удаления всех кругов и др. фигур, показывающих объекты на карте
				removeAllCirclles();


				// Написать более коротко, а все соответствия вынести в отдельный объект, возвращаемый функцией,  как для линий Пешком/На авто
				switch(file_name) {
					case 'm_demeevka':
						demeevka_circle.setMap( map );
						break;
					case 'vern':
						vern_circle.setMap( map );
						break;
					case 'eco':
						eco_circle.setMap( map );
						break;
					case 'bc7':
						bc7_circle.setMap( map );
						break;
					case 'gol13':
						gol13_circle.setMap( map );
						break;
					case 'park':
						park_circle.setMap( map );
						break;
					case 'autos':
						autos_circle.setMap( map );
						break;
					case 'main':
						main_circle.setMap( map );
						break;
					case 'golosPl':
						golosPl_circle.setMap( map );
						break;
					case 'church':
						church_circle.setMap( map );
						break;
					case 'kuz':
						kuz_circle.setMap( map );
						break;
					case 'police':
						police_circle.setMap( map );
						break;
					case 'avtokzal':
						avtokzal_circle.setMap( map );
						break;
				}	
			  
				
				/* sf_popup.stop().animate({
					opacity: 1
				}, 500 ); */
			}
			else {
			/* 	sf_popup.stop().animate({
					opacity: 0
				}, 500, function(){ */
					sf_popup.css({
						display: "none",
						opacity: 0
					});
					sf_popup.html("");
					
					//if (file_name == "m_demeevka") {
                        
                        removeAllCirclles();
                    
						//console.log("yyyyyyyyyyyy");
						// demeevka_circle.setVisible( false );
					//}
                // });
                
			}
			
			
			
		});
		$("#gmr_content").find(".gmr_content_items > div").on("mouseout", function(e){
			
		/* 	sf_popup.stop().animate({
					opacity: 0
				}, 500, function(){ */
					sf_popup.css({
						display: "none",
						opacity: 0
					});
					sf_popup.html("");
				// });
			
		});
		
			
			function getMapDataFromTabs() {
				var data = {
					"gmr_content_p" : {
						"zoom" : 16,
						"center_x" : 50.40230859361056,
						"center_y" : 30.522851943969727,
						"anim_steps" : 30,
						"lines" : function() {
							removeAllPath();
							p_total_path_1.setMap( null );
							p_total_path_2.setMap( null );
							a_total_path_1.setMap( null );
							
							p_total_path_1.setMap(map);
							p_total_path_2.setMap(map);
						}
					},
					"gmr_content_a" : {
						"zoom" : 16,
						"center_x" : 50.40135119612643,
						"center_y" : 30.521607398986816,
						"anim_steps" : 30,
						"lines" : function() {
							removeAllPath();
							p_total_path_1.setMap( null );
							p_total_path_2.setMap( null );
							a_total_path_1.setMap( null );
							
							a_total_path_1.setMap(map);
							p_total_path_2.setMap(map);
						}
					}
				}
				return data;
				
			}
			
        }
		

        google.maps.event.addDomListener(window, 'load', initialize);
		


    });
})(jQuery);