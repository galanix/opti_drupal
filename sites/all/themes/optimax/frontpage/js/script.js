(function($) {
	$(document).ready(function() {
	
	


		front_menu_icon = $("#front_menu_icon");
		front_sidebar = $("#front_sidebar");
		
		points_wrapper_info_close = $("#points_wrapper_info_close");
		points_wrapper_info = $("#points_wrapper_info");

		front_sidebar.addClass('expanded');
		front_menu_icon.addClass('active');

		front_menu_icon.on("click", function(){
			var menu_icon = $( this );
			if (menu_icon.hasClass("active")) {
				menu_icon.removeClass("active");
				front_sidebar.removeClass("expanded");
			}
			else {
				menu_icon.addClass("active");
				front_sidebar.addClass("expanded");
			}
		});



		$('#front_sidebar .menu li.expanded').append('<div class="arrow-block"><span class="arrowFP"></span></div>');

		$('.arrow-block').on('click', function(e) {
			$(this).toggleClass('active');
			$(this).siblings('#front_sidebar .menu li.expanded ul').slideToggle();
			// var event =
			// if(e.target.not($(this))){
			// 	console.log($(this));
			// 	$(this).siblings('#front_sidebar .menu li.expanded ul').slideUp();
			// }
		});


		$('.slide_dot_wrap').on("click", function(){
			var dot = $( this );
			if (dot.hasClass("active")) {
				dot.removeClass("active");
				dot.siblings("div.slide_text_wrap").removeClass("expanded");
			}
			else {
				dot.addClass("active");
				dot.siblings("div.slide_text_wrap").addClass("expanded");
			}
		});
	
	
	points_wrapper_info_close.on('click', function(e) {
		points_wrapper_info.stop().animate({
			opacity: 0
		}, 500, function(){
			points_wrapper_info.hide();
		});
		
	});
		


	});
}) (jQuery);