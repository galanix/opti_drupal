# Optimax #

### Below you can find necessary steps to get application up and running. ###

* Clone the project into the new apache project folder
* Import the .mysql.zip file from project's root into your mysql server (if you do not have yet a db, create one)
* In /sites/default/settings.php change parameters to connect to DB
* Access the website. (preferred with apache webserver)
* Login as admin(uid 1) with drush -l http://localhost uli